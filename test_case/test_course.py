# coding=utf-8
# @time   :2021/08/05
# @Author :wushaotang
# @File   : test_course.py
# @Description: 中教云智慧教学平台-备授课
# @Return  :



import allure
import pytest
import test_datas.login_datas as LD
import test_datas.common_datas as CD
from page_objects.course_page import CoursePage
from utils.UI.BasePage import BasePage


@pytest.mark.skip(reason = '跳过备授课页面的测试')
@allure.feature('测试添加翻转/自定义课程的类')
class TestCourse:

    @pytest.mark.run(order = 1)
    @allure.story('备授课页面')
    @allure.severity(allure.severity_level.CRITICAL)   #发生BUG时的严重程度
    @pytest.mark.parametrize('login_message', LD.login_data)
    def test_course_list(self, common_browser, login_message):
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        course_page = CoursePage(common_browser)
        #进入备授课页面

        #     #创建翻转、自定义课程
        course_page.my_courses()
        course_page.new_courses()
        course_page.add_course()
        course_page.add_course_2()

        #
        # #给课程添加资源
        # course_page.add_resource()