# coding: utf-8
import random
from page_objects.homework_page import HomeworkPage
import allure
import pytest
import test_datas.common_datas as CD
from utils.UI.BasePage import BasePage
from time import sleep


# @pytest.mark.skip(reason='跳过作业列表页面测试')
@allure.feature("测试作业列表功能的类")  # 归为大类
class TestHomework:

    # @pytest.mark.run(order=1)
    @allure.story("作业列表页面")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_homework_list(self, common_browser):
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        homework_page = HomeworkPage(common_browser)
        # 生成所选作业的随机数
        count = homework_page.homework_count()
        index = random.randint(1, count - 1)
        index = str(index)
        print('生成的是第 ' + index + '个作业')
        homework_page.homework_name(index)
        homework_page.course_name(index)
        homework_page.exam_answer_type(index)
        homework_page.homework_content(index)
        homework_page.operate(index)
