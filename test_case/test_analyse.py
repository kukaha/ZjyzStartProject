# coding=utf-8
# @time   :2021/08/25
# @Author :wushaotang
# @File   : test_analyse.py
# @Description: 中教云智慧教学平台-学情分析
# @Return  :



import allure
import pytest
import test_datas.login_datas as LD
import test_datas.common_datas as CD
from page_objects.analyse_page import AnalysePage
from utils.UI.BasePage import BasePage


@pytest.mark.skip(reason = '跳过学情分析的测试')
@allure.feature('测试学情分析功能的类')
class TestAnalyse:

    @pytest.mark.run(order = 1)
    @allure.story('学情分析')
    @allure.severity(allure.severity_level.CRITICAL)   #发生BUG时的严重程度
    @pytest.mark.parametrize('login_message', LD.login_data)
    def test_student_analyse(self, common_browser, login_message):
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)

        analyse_page = AnalysePage(common_browser)
        analyse_page.my_analyse()       #进入学情分析页面
        analyse_page.is_subject()
        analyse_page.is_tab_student()