from page_objects.book_page import BookPage
import allure
import pytest
import test_datas.common_datas as CD
import test_datas.login_datas as LD
from page_objects.faq_page import FAQPage
from page_objects.home_page import HomePage
from utils.UI.BasePage import BasePage


@pytest.mark.skip(reason='跳过帮助中心页测试')
@allure.feature('测试帮助中心的类')  # 归为大类
class TestFAQ:

    @allure.story('帮助中心功能用例')  #归为子类
    @allure.severity(allure.severity_level.CRITICAL)  #发生BUG时的严重程度
    @pytest.mark.parametrize('login_message', LD.login_data)
    def test_ergodic_menu(self, common_browser, login_message):
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        faq_page = FAQPage(common_browser)
        faq_page.faq()