# coding=utf-8
# @time   :2021/08/27
# @Author :wushaotang
# @File   : test_dev_course.py
# @Description: 中教云智慧教学平台-开发-备授课
# @Return  :



import allure
import pytest
import test_datas.login_datas as LD
import test_datas.common_datas as CD
from page_objects.dev_course_page import DevCoursePage
from utils.UI.BasePage import BasePage


# @pytest.mark.skip(reason = '跳过 开发-备授课 的测试')
@allure.feature('测试 开发-备授课 功能的类')
class TestDevCourse:

    @pytest.mark.run(order = 1)
    @allure.story('开发-课程')
    @allure.severity(allure.severity_level.CRITICAL)   #发生BUG时的严重程度
    @pytest.mark.parametrize('login_message', LD.login_data)
    def test_dev_course(self, common_browser, login_message):
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.dev_url, CD.dev_t3_token)
        dev_course_page = DevCoursePage(common_browser)
        dev_course_page.init_dev_course()
        dev_course_page.recommend_course()
        dev_course_page.course_share()
        dev_course_page.revortion_share()
        dev_course_page.teaching_plan()