# coding: utf-8
from page_objects.login_page import LoginPage
from page_objects.login_page import HomePage
import allure
import pytest
import test_datas.login_datas as LD
import test_datas.common_datas as CD
from time import sleep


@pytest.mark.skip(reason='登录功能不再单独测试')
@allure.feature("测试登录功能的类")  # 归为大类
class TestLogin:

    @allure.story("登录用例")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    @pytest.mark.parametrize("login_message", LD.login_data)
    def test_login(self, common_browser, login_message):
        common_browser.get(CD.base_url)
        page = LoginPage(common_browser)
        # 登录智慧教学平台后退出测试数字教材云平台的登录
        page.login_school(username=login_message['username'], password=login_message['password'])
        page.logout()
        page.login(username=login_message['username'], password=login_message['password'])
        home_page = HomePage(common_browser)
        assert home_page.get_img()

    @allure.story("首次登录用例")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    @pytest.mark.parametrize("login_message", LD.teacher_login_data)
    def test_first_login(self, common_browser, login_message):
        common_browser.get(CD.teacher_base_url)
        page = LoginPage(common_browser)
        page.teacher_login(teacher_username=login_message['teacher_username'], teacher_password=login_message['teacher_password'])


if __name__ == '__main__':
    pytest.main()

