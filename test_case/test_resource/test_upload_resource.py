# coding: utf-8
from page_objects.resource_page.upload_resource_page import UploadResourcePage
from utils.UI.BasePage import BasePage
import allure
import pytest
import test_datas.common_datas as CD


# @pytest.mark.skip(reason='跳过上传资源测试')
@allure.feature("测试上传资源功能的类")  # 归为大类
class TestUploadResource:

    # @pytest.mark.run(order=1)
    @allure.story("上传资源")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_upload_resource(self, common_browser):
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        upload_resource_page = UploadResourcePage(common_browser)
        # 上传资源
        upload_resource_page.to_resource_page()
        upload_resource_page.upload_resource()
