# coding: utf-8
from utils.Others.logOperation import logger
from page_objects.resource_page.cloud_resource_page import CloudResourcePage
from utils.UI.BasePage import BasePage
import allure
import pytest
import test_datas.common_datas as CD


# @pytest.mark.skip(reason='跳过云平台资源测试')
@allure.feature("测试资源库云平台功能的类")  # 归为大类
class TestCloudResource:

    # @pytest.mark.run(order=9)
    @allure.story("云平台资源-收藏")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_collect_resource(self, common_browser):
        self.log = logger()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        cloud_resource_page = CloudResourcePage(common_browser)
        cloud_resource_page.collect_resource()

    # @pytest.mark.run(order=10)
    @allure.story("云平台资源-查看资源")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_cloud_resource(self, common_browser):
        cloud_resource_page = CloudResourcePage(common_browser)
        cloud_resource_page.cloud_resource()

    # @pytest.mark.run(order=11)
    @allure.story("云平台资源-资源库列表添加")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_cloud_add(self, common_browser):
        cloud_resource_page = CloudResourcePage(common_browser)
        cloud_resource_page.cloud_add()
    #
    # @pytest.mark.run(order=12)
    @allure.story("云平台资源-资源详情添加")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_resource_detail_add(self, common_browser):
        cloud_resource_page = CloudResourcePage(common_browser)
        cloud_resource_page.resource_detail_add()

    # @pytest.mark.run(order=13)
    @allure.story("云平台资源-资源详情收藏")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_resource_detail_collect(self, common_browser):
        cloud_resource_page = CloudResourcePage(common_browser)
        cloud_resource_page.resource_detail_collect()

    # @pytest.mark.run(order=14)
    @allure.story("云平台资源-资源详情二维码")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_resource_qr_code(self, common_browser):
        cloud_resource_page = CloudResourcePage(common_browser)
        cloud_resource_page.resource_qr_code()

    # @pytest.mark.run(order=15)
    @allure.story("云平台资源-资源详情纠错")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_resource_error_recovery(self, common_browser):
        cloud_resource_page = CloudResourcePage(common_browser)
        cloud_resource_page.resource_error_recovery()

    # @pytest.mark.run(order=16)
    @allure.story("云平台资源-推荐资源")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_resource_recommend(self, common_browser):
        cloud_resource_page = CloudResourcePage(common_browser)
        cloud_resource_page.resource_recommend()