# coding: utf-8
from utils.Others.logOperation import logger
from page_objects.resource_page.cloud_course_page import CloudCoursePage
from utils.UI.BasePage import BasePage
import allure
import pytest
import test_datas.common_datas as CD


# @pytest.mark.skip(reason='跳过云平台课程测试')
@allure.feature("测试资源库云平台功能的类")  # 归为大类
class TestCloudCourse:

    # @pytest.mark.run(order=2)
    @allure.story("云平台收藏课程")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_collect_course(self, common_browser):
        self.log = logger()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        cloud_course_page = CloudCoursePage(common_browser)
        cloud_course_page.to_resource_page()
        cloud_course_page.collect_course()

    # @pytest.mark.run(order=3)
    @allure.story("云平台进入课程详情")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_cloud_course(self, common_browser):
        cloud_course_page = CloudCoursePage(common_browser)
        cloud_course_page.cloud_course()

    # @pytest.mark.run(order=4)
    @allure.story("云平台资源-查看课程")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_course_detail(self, common_browser):
        cloud_course_page = CloudCoursePage(common_browser)
        cloud_course_page.course_detail()

    # @pytest.mark.run(order=5)
    @allure.story("云平台资源-课程创建人信息")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_course_creator(self, common_browser):
        cloud_course_page = CloudCoursePage(common_browser)
        cloud_course_page.course_creator()

    # @pytest.mark.run(order=6)
    @allure.story("云平台资源-课程历史版本记录")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_history_version(self, common_browser):
        cloud_course_page = CloudCoursePage(common_browser)
        cloud_course_page.history_version()

    # @pytest.mark.run(order=7)
    @allure.story("云平台资源-引用课程")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_use_course(self, common_browser):
        cloud_course_page = CloudCoursePage(common_browser)
        cloud_course_page.use_course()

    # @pytest.mark.run(order=8)
    @allure.story("云平台资源-查看推荐课程")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_recommend_course(self, common_browser):
        cloud_course_page = CloudCoursePage(common_browser)
        cloud_course_page.check_recommend_course()