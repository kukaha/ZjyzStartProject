# coding=utf-8
# @time   :2021/08/13
# @Author :wushaotang
# @File   : test_add_resource.py
# @Description: 中教云智慧教学平台-备授课-添加资源
# @Return  :



import allure
import pytest
import test_datas.login_datas as LD
import test_datas.common_datas as CD
from page_objects.course_page import CoursePage
from page_objects.add_resource_page import AddResourcePage
from utils.UI.BasePage import BasePage


@pytest.mark.skip(reason = '跳过课程-添加资源的测试')
@allure.feature('测试添加资源功能的类')
class TestAddresource:

    @pytest.mark.run(order = 1)
    @allure.story('课程添加资源')
    @allure.severity(allure.severity_level.CRITICAL)   #发生BUG时的严重程度
    @pytest.mark.parametrize('login_message', LD.login_data)
    def test_course_list(self, common_browser, login_message):
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        course_page = CoursePage(common_browser)
        addresour_page = AddResourcePage(common_browser)
        #进入备授课页面
        course_page.my_courses()
        #给课程添加资源
        addresour_page.add_resource()