# coding=utf-8
# @time   :2021/08/26
# @Author :wushaotang
# @File   : test_errors_set.py
# @Description: 中教云智慧教学平台-学生错题集
# @Return  :



import allure
import pytest
import test_datas.login_datas as LD
import test_datas.common_datas as CD
from page_objects.analyse_page import AnalysePage
from page_objects.errors_set_page import ErrorsSerPage
from utils.UI.BasePage import BasePage


@pytest.mark.skip(reason = '跳过学生错题集的测试')
@allure.feature('测试 学生错题集 功能的类')
class TestErrorsSet:

    @pytest.mark.run(order = 1)
    @allure.story('学生错题集')
    @allure.severity(allure.severity_level.CRITICAL)   #发生BUG时的严重程度
    @pytest.mark.parametrize('login_message', LD.login_data)
    def test_errors_set(self, common_browser, login_message):
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        errorsset_page = ErrorsSerPage(common_browser)
        errorsset_page.my_errors()
        errorsset_page.is_left_menu()