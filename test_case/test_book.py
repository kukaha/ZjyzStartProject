# coding: utf-8
from page_objects.book_page import BookPage
import allure
import pytest
import test_datas.login_datas as LD
import test_datas.common_datas as CD
from time import sleep

from utils.UI.BasePage import BasePage


@pytest.mark.skip(reason='跳过数字教材页测试')
@allure.feature("测试数字教材的类")  # 归为大类
class TestBook:

    # @pytest.mark.run(order=1)
    @allure.story("数字教材功能用例")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_open_book(self, common_browser):
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        book_page = BookPage(common_browser)
        # 选择一本教材点击打开
        assert book_page.screen_book()
        book_page.open_book()
