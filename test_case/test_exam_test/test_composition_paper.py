# coding: utf-8
from utils.Others.logOperation import logger
from page_objects.exam_test_page.generate_exam_page import GenerateExamPage
from utils.UI.BasePage import BasePage
import allure
import pytest
import test_datas.common_datas as CD


# @pytest.mark.skip(reason='跳过云题库组卷测试')
@allure.feature("测试题库组卷的类")  # 归为大类
class TestCompositionPaper:

    @allure.story("题库组卷页面，试题解析")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_test_analysis(self, common_browser):
        self.log = logger()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        generate_exam_page = GenerateExamPage(common_browser)
        generate_exam_page.test_analysis()

    @allure.story("题库组卷页面，编辑试题")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_edit_test(self, common_browser):
        generate_exam_page = GenerateExamPage(common_browser)
        generate_exam_page.edit_test()

    @allure.story("题库组卷页面，设置分数")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_set_score(self, common_browser):
        generate_exam_page = GenerateExamPage(common_browser)
        generate_exam_page.set_score()

    @allure.story("题库组卷页面，插入作答区域")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_insert_answer_area(self, common_browser):
        generate_exam_page = GenerateExamPage(common_browser)
        generate_exam_page.insert_answer_area()

    @allure.story("题库组卷页面，收藏试题")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_collect_test(self, common_browser):
        generate_exam_page = GenerateExamPage(common_browser)
        generate_exam_page.collect_test()

    @allure.story("题库组卷页面，试题纠错")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_test_correct_error(self, common_browser):
        generate_exam_page = GenerateExamPage(common_browser)
        generate_exam_page.test_correct_error()

    @allure.story("题库组卷页面，删除试题")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_delete_test(self, common_browser):
        generate_exam_page = GenerateExamPage(common_browser)
        generate_exam_page.delete_test()