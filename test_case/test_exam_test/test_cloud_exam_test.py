# coding: utf-8
from utils.Others.logOperation import logger
from page_objects.exam_test_page.cloud_test_page import CloudTestPage
from utils.UI.BasePage import BasePage
import allure
import pytest
import test_datas.common_datas as CD


# @pytest.mark.skip(reason='跳过云平台题库测试')
@allure.feature("测试云平台题库的类")  # 归为大类
class TestCloudExamTest:

    # @pytest.mark.run(order=1)
    @allure.story("题库云平台-引用的试卷")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_use_question(self, common_browser):
        self.log = logger()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        cloud_test_page = CloudTestPage(common_browser)
        cloud_test_page.use_exam()

    @allure.story("题库云平台-收藏试题")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_collect_question(self, common_browser):
        self.log = logger()
        cloud_test_page = CloudTestPage(common_browser)
        cloud_test_page.collect_question()

    @allure.story("题库云平台-查看试题解析")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_question_analysis(self, common_browser):
        self.log = logger()
        cloud_test_page = CloudTestPage(common_browser)
        cloud_test_page.question_analysis()

    @allure.story("题库云平台-添加试题到试题篮")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_add_basket(self, common_browser):
        self.log = logger()
        cloud_test_page = CloudTestPage(common_browser)
        cloud_test_page.add_basket()