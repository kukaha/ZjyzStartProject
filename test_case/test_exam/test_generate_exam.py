# coding: utf-8
from utils.Others.logOperation import logger
from page_objects.exam_page.stage_exam_page import StageExamPage
from page_objects.exam_page.generate_exam_page import GenerateExamPage
from utils.UI.BasePage import BasePage
import allure
import pytest
import test_datas.common_datas as CD


# @pytest.mark.skip(reason='跳过生成试卷测试')
@allure.feature("测试生成试卷的类")  # 归为大类
class TestGenerateExam:

    # @pytest.mark.run(order=1)
    @allure.story("智能组卷-知识点组卷，交叉设置题量生成试卷")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_generate_knowledge_exam(self, common_browser):
        self.log = logger()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.dev_url, CD.dev_token)
        stage_exam_page = StageExamPage(common_browser)
        generate_exam_page = GenerateExamPage(common_browser)
        stage_exam_page.to_exam_page()
        generate_exam_page.generate_knowledge_exam()

    # @pytest.mark.run(order=1)
    @allure.story("智能组卷-章节组卷，自由设置题量生成试卷")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_generate_catalog_exam(self, common_browser):
        self.log = logger()
        stage_exam_page = StageExamPage(common_browser)
        generate_exam_page = GenerateExamPage(common_browser)
        stage_exam_page.to_exam_page()
        generate_exam_page.generate_catalog_exam()