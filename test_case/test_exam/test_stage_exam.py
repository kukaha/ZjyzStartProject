# coding: utf-8
from utils.Others.logOperation import logger
from page_objects.exam_page.stage_exam_page import StageExamPage
from utils.UI.BasePage import BasePage
import allure
import pytest
import test_datas.common_datas as CD


@pytest.mark.skip(reason='跳过试卷库阶段考试测试')
@allure.feature("测试试卷库阶段考试的类")  # 归为大类
class TestStageExam:

    # @pytest.mark.run(order=1)
    @allure.story("阶段考试布置作业")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_assign_homework(self, common_browser):
        self.log = logger()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        stage_exam_page = StageExamPage(common_browser)
        stage_exam_page.to_exam_page()
        stage_exam_page.assign_homework()

    # @pytest.mark.run(order=2)
    @allure.story("阶段考试打印")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_print_exam(self, common_browser):
        self.log = logger()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        stage_exam_page = StageExamPage(common_browser)
        stage_exam_page.to_exam_page()
        stage_exam_page.print_exam()

    # @pytest.mark.run(order=3)
    @allure.story("阶段考试添加到课程")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_add_exam(self, common_browser):
        self.log = logger()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        stage_exam_page = StageExamPage(common_browser)
        stage_exam_page.to_exam_page()
        stage_exam_page.add_exam_to_course()

    # @pytest.mark.run(order=4)
    @allure.story("阶段考试收藏")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_collect_exam(self, common_browser):
        self.log = logger()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        stage_exam_page = StageExamPage(common_browser)
        stage_exam_page.to_exam_page()
        stage_exam_page.collect_exam()

    # @pytest.mark.run(order=5)
    @allure.story("试卷详情试题信息")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_info_test_detail(self, common_browser):
        self.log = logger()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        stage_exam_page = StageExamPage(common_browser)
        stage_exam_page.to_exam_page()
        stage_exam_page.info_test_detail()

    # @pytest.mark.run(order=6)
    @allure.story("试卷详情显示答案")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_info_answer_on_off(self, common_browser):
        self.log = logger()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        stage_exam_page = StageExamPage(common_browser)
        stage_exam_page.to_exam_page()
        stage_exam_page.info_answer_on_off()

    # @pytest.mark.run(order=7)
    @allure.story("试卷详情复制试卷")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_info_copy_exam(self, common_browser):
        self.log = logger()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        stage_exam_page = StageExamPage(common_browser)
        stage_exam_page.to_exam_page()
    #     stage_exam_page.info_copy_exam()

    # @pytest.mark.run(order=8)
    @allure.story("试卷详情添加试卷到课程")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_info_add_exam(self, common_browser):
        self.log = logger()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        stage_exam_page = StageExamPage(common_browser)
        stage_exam_page.to_exam_page()
        stage_exam_page.info_add_to_course()