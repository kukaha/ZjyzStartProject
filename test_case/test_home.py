# coding: utf-8
from page_objects.home_page import HomePage
import allure
import pytest
import test_datas.login_datas as LD
import test_datas.common_datas as CD
from utils.UI.BasePage import BasePage
from time import sleep


# @pytest.mark.skip(reason='跳过首页测试')
@allure.feature("测试首页功能的类")  # 归为大类
class TestHome:

    # @pytest.mark.run(order=1)
    @allure.story("首页系统更新说明用例")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_update_explain(self, common_browser):
        # common_browser.get(CD.base_url)
        # login_page = LoginPage(common_browser)
        # login_page.login_school(username=login_message['username'], password=login_message['password'])
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        home_page = HomePage(common_browser)
        assert home_page.get_img()
        home_page.update_explain()

    # @pytest.mark.run(order=2)
    @allure.story("首页跳转列表页功能用例")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_jump(self, common_browser):
        home_page = HomePage(common_browser)
        # 数字教材页
        home_page.more_book()
        # 批改作业页
        home_page.correct_homework()
        # 资源库
        home_page.resource_lib()
        # 试卷库
        home_page.exam_lib()
        # sleep(2)
        # 题库
        home_page.test_lib()
        # 全局搜索
        home_page.global_search()
        sleep(2)

    # @pytest.mark.run(order=3)
    @allure.story("首页去备课功能用例")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    @pytest.mark.parametrize("login_message", LD.login_data)
    def test_to_prepare_class(self, common_browser, login_message):
        home_page = HomePage(common_browser)
        home_page.goto_prepare_class()

    # @pytest.mark.run(order=4)
    @allure.story("首页课程上课和备课功能用例")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    @pytest.mark.parametrize("login_message", LD.login_data)
    def test_to_class(self, common_browser, login_message):
        home_page = HomePage(common_browser)
        # 上课
        home_page.click_goto_class()
        # 今日日程
        home_page.goto_programme()
        # 备课
        home_page.prepare_class()

    # @pytest.mark.run(order=5)
    @allure.story("首页国培活动用例")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    @pytest.mark.parametrize("login_message", LD.login_data)
    def test_active(self, common_browser, login_message):
        home_page = HomePage(common_browser)
        home_page.active()


if __name__ == '__main__':
    pytest.main()
