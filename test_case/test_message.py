# coding: utf-8
from page_objects.message_page import MessagePage
from utils.UI.BasePage import BasePage
import allure
import pytest
import test_datas.login_datas as LD
import test_datas.common_datas as CD
from time import sleep


@pytest.mark.skip(reason='跳过消息中心测试')
@allure.feature("测试消息中心功能的类")  # 归为大类
class TestMessage:

    # @pytest.mark.run(order=5)
    @allure.story("消息中心")  # 归为子类
    @allure.severity(allure.severity_level.CRITICAL)  # 发生BUG时的严重程度
    def test_message(self, common_browser):
        # common_browser.get(CD.base_url)
        # login_page = LoginPage(common_browser)
        # login_page.login_school(username=login_message['username'], password=login_message['password'])
        # assert home_page.get_img()
        base_page = BasePage(common_browser)
        base_page.exempt_login(CD.pre_url, CD.pre_token)
        message_page = MessagePage(common_browser)
        message_page.click_message_link()
