# coding:utf-8
# ==============================
#         生成报告的封装
# ==============================
import os
import shutil
import subprocess
from config.globalVars import G
from utils.Others.logOperation import logger


class reportManager:
    def __init__(self):
        self.log = logger()

    def run_bat(self, file):
        p = subprocess.Popen("cmd.exe /c" + file, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        curline = p.stdout.readline()
        while (curline != b''):
            print(curline)
            curline = p.stdout.readline()
        p.wait()
        print(p.returncode)

    def del_old_result(self):
        self.log.info("删除旧的结果集……")
        if os.path.exists(G.REPORT_RESULT_PATH):
            shutil.rmtree(G.REPORT_RESULT_PATH)

    def del_old_screenshot(self):
        self.log.info("删除旧的截图……")
        if os.path.exists(G.SCREENSHOT_DIR):
            shutil.rmtree(G.SCREENSHOT_DIR)

    def generate_report(self, driver_info):
        self.log.info("生成报告……")
        result_path = os.path.join(G.REPORT_RESULT_PATH, driver_info)
        report_path = os.path.join(G.REPORT_END_PATH, driver_info)
        os.system(f"allure generate {result_path} -o {report_path} --clean")
        # 复制history文件夹，在本地生成趋势图
        if not os.path.exists(G.REPORT_HISTORY_PATH):
            os.mkdir(G.REPORT_HISTORY_PATH)
        files = os.listdir(G.REPORT_HISTORY_PATH)
        result_history_dir = os.path.join(G.REPORT_RESULT_PATH, driver_info, "history")
        # 如果不存在则先创建文件夹
        if not os.path.exists(result_history_dir):
            os.mkdir(result_history_dir)
        for file in files:
            shutil.copy(os.path.join(G.REPORT_HISTORY_PATH, file), result_history_dir)

    def run_allure_server(self):
        self.log.info("启动allure服务！")
        os.system(f"allure open {G.REPORT_RESULT_PATH}")
