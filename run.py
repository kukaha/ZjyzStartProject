# coding: utf-8
import pytest
from multiprocessing import Pool
from utils.Others.reportOperation import reportManager

# 驱动信息可传入多个驱动,同时运行
driver_infos = [
    "chrome",
]


def run_parallel(driver_info):
    # 开始运行测试用例
    pytest.main([f"--cmdopt={driver_info}", "--alluredir", f"./report/allure_result/{driver_info}"])
    # 报告工具类
    manager = reportManager()
    # 生成测试报告
    manager.generate_report(driver_info)


# 使用Pool进程池可进行多进程运行,同时在多个浏览器执行测试用例
def pytest_start():
    with Pool(len(driver_infos)) as pool:
        pool.map(run_parallel, driver_infos)
        pool.close()
        pool.join()


# 程序的入口
if __name__ == '__main__':
    pytest_start()
