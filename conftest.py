# coding: utf-8
import os
import pytest
from config.browser import Browser
from utils.Others.logOperation import logger
from time import sleep
from selenium import webdriver
from config.globalVars import G

log = logger()


# pytest配置对象
# 注意此对象只能在顶层目录的conftest.py文件中完成
# action：store 存储参数
# default 参数默认值
# help 参数帮助信息，此处为无
# pytest内置函数,可添加自定义参数
def pytest_addoption(parser):
    parser.addoption("--cmdopt", action="store",
                     default="None",
                     help="将自定义命令行参数 '--cmdopt' 添加到 pytest 配置中")


# 从配置对象获取 cmdopt 的值
# 然后任何 fixture 或测试用例都可以调用 cmdopt 来获得设备信息
# pytest内置参数pytestconfig
@pytest.fixture(scope="package")
def cmdopt(pytestconfig):
    return pytestconfig.getoption("--cmdopt")


@pytest.fixture(scope="package")
def common_browser(cmdopt):
    os.system('chcp 65001')
    # 若驱动不存在则下载
    if G.DRIVER_PATH is None:
        Browser.set_browser()
    if cmdopt == "chrome":
        driver_path = G.DRIVER_PATH
        browser = webdriver.Chrome(executable_path=driver_path)
    else:
        raise Exception
    browser.implicitly_wait(10)
    browser.maximize_window()
    yield browser
    browser.quit()
