# coding=utf-8
# @time   :2021/07/27
# @Author :wushaotang
# @File   : course_page.py
# @Description: 中教云智慧教学平台-备授课
# @Return  :
import re
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from utils.UI.BasePage import BasePage
from page_locators.course_locators import CourseLocators as cl


class CoursePage(BasePage):
    def __init__(self, driver: webdriver.Remote):
        super().__init__(driver)
        self._count = 0

    def count_coursers(self):
        '''
        若有显示总计，则返回总计，若没有总计显示，则返回当前元素长度
        :return: 获取当前课程列表的总数
        '''
        if self.isElementExist(cl.t_count_coursers):
            try:
                n_count = self.find_element(cl.t_count_coursers).text
                self.log.info('当前课程列表共{}节'.format(re.findall('\d+',n_count)[0]))
                return int(re.findall('\d+',n_count)[0])

            except Exception as e :
                return 0
        else:
            n_count = self.count_elements(cl.d_courses)
            self.log.info('当前课程列表共{}节'.format(n_count))
            return n_count


    def my_courses(self):
        '''
        备授课页面的初始化
        '''
        try:
            self.find_element(cl.menu_course).click()       #点击菜单‘备授课’
            self.find_element(cl.left_courser).click()        #确认左侧菜单的课程
            self.get_ele_count(cl.d_courses)    #获取当前列表页的课程列表
            self._count = self.count_coursers()     #获取当前课程总数，并写入公共变量
        except Exception as e:
            self.log.info('备授课页面初始化失败')
            self.get_screenshot_name('备授课页面初始化失败')

    def new_courses(self):
        '''
        弹框新增课程
        :return:
        '''
        try:
            self.find_element(cl.d_new_courses).click()     # 点击新建课程，会有弹框
            self.find_element(cl.i_new_zhangjie).click()      #点击弹框的‘章节’下拉框
            self.find_element(cl.i_new_zhangjie).click()  # 点击弹框的‘章节’下拉框
            # self.find_element(cl.i_zhangjie_1).click()
            # self.send_keys(cl.i_courser_name,  '自动化测试数据{}'.format(time.strftime("%Y-%m-%d %H:%M", time.localtime())))
            # time.sleep(1)
            # self.execute_js("document.getElementById('add-course_courseName').value='{}';".format(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())))
            # self.find_element(cl.i_courser_name).click()
            # self.execute_js("document.getElementById('add-course_courseName').value='{}';".format('aaawwaa'))
            self.find_element(cl.i_courser_name).send_keys(Keys.CONTROL, "a")
            self.find_element(cl.i_courser_name).send_keys(Keys.DELETE)
            self.find_element(cl.i_courser_name).send_keys('{}'.format(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())))
        except Exception as e :
            self.get_screenshot_name('新增课程弹框元素选择失败')
            raise e

    def add_course(self):
        '''
        翻转课堂的创建
        :return:
        '''
        try:
            self.find_element(cl.lab_rotate).click()        #选择翻转课堂
            self.find_element(cl.b_submit).click()          #提交
            time.sleep(2.5)
            assert self._count +1 == self.count_coursers()
        except Exception as e :
            self.get_screenshot_name('新增翻转课程失败')
            raise  e

    def add_course_2(self):
        '''
        自定义课堂的创建
        :return:
        '''
        try:
            self.my_courses()
            self.new_courses()
            self.find_element(cl.lab_user_defined) .click()     #选择自定义
            self.find_element(cl.b_submit).click()
            time.sleep(2)
            assert  self._count + 1 == self.count_coursers()

        except Exception as e :
            self.get_screenshot_name('新增自定义课程失败')
            raise e