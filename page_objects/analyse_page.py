# coding=utf-8
# @time   :2021/08/25
# @Author :wushaotang
# @File   : analyse_page.py
# @Description: 中教云智慧教学平台-学情分析
# @Return  :


import re
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from utils.UI.BasePage import BasePage
from page_locators.analyse_locators import AnalyseLocators as al

class AnalysePage(BasePage):
    def __init__(self, driver: webdriver.Remote):
        super().__init__(driver)


    def my_analyse(self):
        '''
        学情分析页面初始化
        '''
        self.find_element(al.menu_analyse).click()

    def is_subject(self):
        '''
        学科学情页面验证
        '''
        # tex = self.find_element(al.ave_time).text

        assert self.find_element(al.ave_time).text == '平均每日在线学习时长'
        assert self.find_element(al.zhangjie).text == '章节掌握情况'

    def is_tab_student(self):
        '''
        学生学情页面验证
        '''
        self.find_element(al.student_analyse).click()
        assert self.find_element(al.answer).text == '知识点分析'
        assert self.find_element(al.student_analysis).text == '学情分析分析'