# coding: utf-8
import random
from time import sleep

from utils.UI.BasePage import BasePage
from page_locators.exam_test_locators.cloud_test_locators import CloudTestLocators as cloudtest


class CloudTestPage(BasePage):

    def to_question_bank(self):
        sleep(1)
        self.find_element(cloudtest.question_bank).click()

    def use_exam(self):
        """
        点击引用的试卷，进入试卷详情页
        """
        self.to_question_bank()
        test_list = self.find_elements(cloudtest.test_list)
        for li_index in range(1, test_list.__len__() + 1):
            test_year_info_text = self.browser.find_element(*cloudtest.test_year_info(str(li_index))).text
            # 有引用试卷时，点击进入试卷详情页
            if test_year_info_text != '':
                sleep(1)
                self.move_to(cloudtest.test_year_info(str(li_index)))
                test_year_info_tip = self.find_element(cloudtest.test_year_info_tip(str(li_index + 5))).text
                self.find_element(cloudtest.test_year_info(str(li_index))).click()
                test_paper_name = self.find_element(cloudtest.test_paper_name).text
                if test_year_info_tip == test_paper_name:
                    self.log.info('进入试卷' + test_paper_name + '详情页')
                    self.find_element(cloudtest.back_btn).click()
            # 遍历引用的试卷列表，进入试卷详情
            use_num = self.find_element(cloudtest.use_question_num(str(li_index))).text
            if use_num != '0':
                self.log.info('有' + use_num + '个试卷引用列表')
                self.move_to(cloudtest.use_question_num(str(li_index)))
                use_question_list = self.find_elements(cloudtest.use_question)
                for use_question in use_question_list:
                    sleep(1)
                    self.move_to(cloudtest.use_question_num(str(li_index)))
                    use_question_text = use_question.text
                    use_question.click()
                    if self.isElementExist(cloudtest.test_paper_name):
                        test_paper_name = self.find_element(cloudtest.test_paper_name).text
                        if use_question_text == test_paper_name:
                            self.find_element(cloudtest.back_btn).click()
                    else:
                        raise Exception('预览试卷失败')
                break

    def question_analysis(self):
        """
        试题解析弹框
        """
        self.to_question_bank()
        sleep(1)
        self.find_element(cloudtest.question_operation(str(1), str(1))).click()
        analysis_detail = self.find_elements(cloudtest.question_analysis)
        if analysis_detail.__len__() != 0:
            self.log.info('试题解析弹框内容显示正常')
            self.find_element(cloudtest.close).click()
        else:
            raise Exception('试题解析内容有误')

    def collect_question(self):
        """
        收藏试题
        """
        self.to_question_bank()
        question_count = self.count_elements(cloudtest.test_list)
        question_index = str(random.randint(1, question_count))
        button_text = self.find_element(cloudtest.question_operation(question_index, str(4))).text
        self.find_element(cloudtest.question_operation(question_index, str(4))).click()
        alert_text = self.find_element(cloudtest.alert).text
        if button_text == '添加收藏' and alert_text == '收藏成功':
            self.log.info('收藏成功')
        elif button_text == '已收藏' and alert_text == '取消收藏成功':
            self.log.info('取消收藏成功')
        else:
            raise Exception('收藏或取消收藏失败')

    def add_basket(self):
        """
        添加试题到试题篮
        """
        self.to_question_bank()
        if self.isElementExist(cloudtest.basket_num(str(2))):
            # if self.isElementExist(cloudtest.test_form_more):
            #     sleep(1)
            #     self.find_element(cloudtest.test_form_more).click()
            sleep(3)
            test_form_list = self.find_elements(cloudtest.test_form_list)
            for test_form_a in range(2, test_form_list.__len__() + 1):
                sleep(1)
                basket_num = int(self.find_element(cloudtest.basket_num(str(1))).text)
                if basket_num < 40:
                    self.find_element(cloudtest.test_form_a(str(test_form_a))).click()
                    sleep(1)
                    test_form_text = self.find_element(cloudtest.test_form_a(str(test_form_a))).text
                    basket_num = int(self.find_element(cloudtest.basket_num(str(1))).text)
                    if basket_num < 40 and self.isElementExist(cloudtest.test_list):
                        self.log.info('试题篮总数小于40，' + test_form_text + '中有题')
                        test_list = self.find_elements(cloudtest.test_list)
                        sleep(1)
                        for li_index in range(1, test_list.__len__() + 1):
                            paper_basket_text = self.find_element(cloudtest.paper_basket_text(str(li_index))).text
                            basket_num = int(self.find_element(cloudtest.basket_num(str(1))).text)
                            if paper_basket_text == '试题篮' and basket_num < 40:
                                sleep(1)
                                self.find_element(cloudtest.paper_basket(str(li_index))).click()
                    continue


