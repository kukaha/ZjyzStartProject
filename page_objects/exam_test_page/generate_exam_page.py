# coding: utf-8
import random
from time import sleep

from utils.UI.BasePage import BasePage
from page_objects.exam_test_page.cloud_test_page import CloudTestPage
from page_locators.exam_test_locators.cloud_test_locators import CloudTestLocators as cloud
from page_locators.exam_test_locators.generate_exam_locators import GenerateExamLocators as generate


class GenerateExamPage(BasePage):

    def generate_exam(self):
        """
        进入组卷页面，随机选中一个小题
        """
        cloud_page = CloudTestPage(self.browser)
        cloud_page.to_question_bank()
        sleep(3)
        basket_num = self.find_element(cloud.basket_num(str(1))).text
        if basket_num != '0':
            self.find_element(generate.basket).click()
            # 组卷页面大题列表
            major_test_list = self.count_elements(generate.major_test_list)
            div_index = str(random.randint(1, major_test_list))
            # 组卷页面小题列表
            test_list = self.count_elements(generate.test_list(div_index))
            div_index1 = str(random.randint(1, test_list))
            self.move_to(generate.test_index(div_index, div_index1))
            return test_list, div_index, div_index1

    def test_analysis(self):
        """
        查看解析
        """
        test_list, div_index, div_index1 = self.generate_exam()
        self.find_element(generate.test_operation(div_index, div_index1, str(1))).click()
        analysis_detail = self.find_elements(cloud.question_analysis)
        if analysis_detail.__len__() != 0:
            self.log.info('试题解析弹框内容显示正常')
            self.find_element(cloud.close).click()
        else:
            raise Exception('试题解析内容有误')

    def edit_test(self):
        """
        编辑试题
        """
        test_list, div_index, div_index1 = self.generate_exam()
        self.find_element(generate.test_operation(div_index, div_index1, str(2))).click()
        if self.isElementExist(generate.edit_test_detail):
            self.find_element(generate.confirm).click()
        else:
            raise Exception('编辑试题有误')

    def set_score(self):
        """
        设定得分
        """
        test_list, div_index, div_index1 = self.generate_exam()
        self.find_element(generate.test_operation(div_index, div_index1, str(3))).click()
        test_score_list = self.find_elements(generate.test_score)
        for test_score in test_score_list:
            test_score.clear()
            test_score.send_keys(2)
        self.find_element(generate.confirm1).click()
        if self.find_element(generate.alert).text == '试卷更新成功！':
            self.log.info('分数设定成功')
        else:
            raise Exception('分数设定失败')

    def insert_answer_area(self):
        """
        插入作答区域
        """
        test_list, div_index, div_index1 = self.generate_exam()
        self.find_element(generate.test_operation(div_index, div_index1, str(4))).click()
        self.find_element(generate.input_answer_area).click()
        self.find_element(generate.input_answer_area).clear()
        self.find_element(generate.input_answer_area).send_keys('1')
        answer_class = self.find_element(generate.answer_line_btn).get_attribute('class')
        if answer_class == 'ant-switch':
            self.find_element(generate.answer_line_btn).click()
            sleep(1)
            if self.isElementExist(generate.answer_line):
                self.log.info('作答线插入成功')
            else:
                raise Exception('作答线为空')

    def collect_test(self):
        """
        收藏取消收藏试题
        """
        test_list, div_index, div_index1 = self.generate_exam()
        collect_test_text = self.find_element(generate.test_operation(div_index, div_index1, str(5))).text
        self.find_element(generate.test_operation(div_index, div_index1, str(5))).click()
        self.move_to(generate.test_index(div_index, div_index1))
        new_collect_test_text = self.find_element(generate.test_operation(div_index, div_index1, str(5))).text
        if (collect_test_text == '收藏' and new_collect_test_text =='取消收藏') or (collect_test_text == '取消收藏' and new_collect_test_text =='收藏'):
            return True
        else:
            raise Exception('收藏试题功能异常')

    def test_correct_error(self):
        """
        试题纠错功能
        """
        test_list, div_index, div_index1 = self.generate_exam()
        self.find_element(generate.test_operation(div_index, div_index1, str(6))).click()
        self.find_element(generate.error_type).click()
        self.find_element(generate.error_description).send_keys('测试纠错描述')
        self.find_element(generate.confirm1).click()

    def delete_test(self):
        """
        删除试题功能
        """
        test_list, div_index, div_index1 = self.generate_exam()
        self.find_element(generate.test_operation(div_index, div_index1, str(7))).click()
        self.log.info(self.find_element(generate.delete_test_message).text)
        self.find_element(generate.confirm).click()
        sleep(1)
        test_count = self.count_elements(generate.test_list(div_index))
        if test_count == test_list - 1:
            self.log.info('试题已删除，列表数据一致')
        else:
            raise Exception('试题删除失败')
