# coding: utf-8
from utils.UI.BasePage import BasePage
from page_locators.message_locators import MessageLocators as ml
from page_locators.breadcrumb_locators import BreadcrumbLocators as bl
import random
from time import sleep


class MessagePage(BasePage):
    def click_message_link(self):
        """
        进入消息中心页面，点击跳转到对应页面
        """
        self.find_element(ml.message_button).click()
        count = self.count_elements(ml.message_title)
        if count > 0:
            index = str(random.randint(2, count + 1))
            message_title = ml.get_message_title(index)
            message_link = ml.get_message_link(index)
            title = self.find_element(message_title).text
            self.log.info("随机选择的标题是：" + title)
            if "系统更新" in title:
                return
            self.find_element(message_link).click()
            if self.ele_is_exist(ml.alert):
                self.log.info("已被删除或不存在")
                return
            message_lists = ["课程", "试题", "资源", "试卷", "奖品"]
            for title_type in message_lists:
                if title_type in title:
                    if title_type == "课程":
                        info_count = self.count_elements(bl.class_info)
                    elif title_type == "试题":
                        info_count = self.count_elements(bl.test_list)
                    elif title_type == "资源":
                        info_count = self.count_elements(bl.resource_info)
                    elif title_type == "试卷":
                        info_count = self.count_elements(bl.exam_info)
                    else:
                        info_count = self.count_elements(bl.points_chage)
                    if info_count > 0:
                        self.log.info("进入了" + title_type + "详情页")
                        self.back_page()
                    else:
                        raise Exception("进入" + title_type + "详情失败")
                    break
