# coding=utf-8
# @time   :2021/08/27
# @Author :wushaotang
# @File   : dev_course_page.py
# @Description: 中教云智慧教学平台-开发-备授课
# @Return  :


import re
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from utils.UI.BasePage import BasePage
from page_locators.dev_course_locators import DevCourseLocators as dl

class DevCoursePage(BasePage):
    def __init__(self, driver: webdriver.Remote):
        super().__init__(driver)

    def init_dev_course(self):
        '''
        初始化页面
        '''
        self.find_element(dl.menu_course).click()

    def recommend_course(self):
        '''
        右侧推荐课程
        '''
        if not self.isElementExist(dl.recommend_course):
            return self.log.info('当前没有推荐课程')
        self.find_element(dl.recommend_course).click()
        course_name = self.find_element(dl.course_title_n).text
        self.log.info('推荐课程的名称是{}'.format(course_name))
        if not self.isElementExist(dl.recommend_resource):      #检查弹出课程弹筐里有没有资源
            self.log.info('弹出的课程里没有资源')
        else:
            self.find_element(dl.recommend_resource).click()
            if self.isElementExist(dl.recommend_resource_close):        #检查弹出的资源是否已打开
                self.log.info('该资源已经成功打开')
                self.find_element(dl.recommend_resource_close).click()      #关闭
            else:
                raise Exception('弹出课程里的资源打开失败')

        self.find_element(dl.recommend_close).click()      #关闭


    def course_share(self):
        '''
        共享课程
        '''
        self.init_dev_course()
        time.sleep(1)
        self.find_element(dl.course_list_1).click()     #进入详情页
        self.find_element(dl.share_butten).click()      #点击共享按钮
        self.find_element(dl.school_share).click()      #'校内共享'
        self.find_element(dl.share_submit).click()
        # assert self.find_element(dl.share_location).text == '共享成功'
        self.log.info('点击共享后的弹窗为“{}”'.format(self.find_element(dl.pop_up).text))
        assert self.find_element(dl.pop_up).text == '共享成功'

        self.log.info('校内共享成功')

    def revortion_share(self):
        '''
        撤销共享
        '''
        self.find_element(dl.share_log).click()
        self.move_to(dl.mouse_location)
        self.find_element(dl.revortion).click()     #’撤销‘共享
        self.find_element(dl.submit).click()        #提交
        self.find_element(dl.share_log).click()
        time.sleep(1)
        self.log.info('撤销共享后的弹窗为“{}”'. format(self.find_element(dl.pop_up).text))
        assert '暂无共享记录' in self.find_element(dl.pop_up).text
        pass

    def teaching_plan(self):
        '''
        教案
        '''
        self.find_element(dl.teaching_plan).click()
        self.find_element(dl.teaching_1).click()        #点击右侧 课件
        assert self.isElementExist(dl.right_one)
        self.log.info('点击右侧课件后，能正常展示课件')
        self.find_element(dl.teaching_3).click()        #点击右侧推荐教案
        assert self.isElementExist(dl.right_one)
        self.log.info('点击右侧 推荐教案 后，能正常展示 推荐教案')
