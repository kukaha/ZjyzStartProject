# coding: utf-8
import os
import time
from time import sleep

from utils.UI.BasePage import BasePage
from page_locators.resource_locators.upload_resource_locators import UploadResourceLocators as url
import random


class UploadResourcePage(BasePage):

    # 点击资源库导航进入资源库页面
    def to_resource_page(self):
        self.find_element(url.resource).click()

    def upload_resource(self):
        """
        资源库上传资源功能：
        打开上传资源弹框直接点击确定，判断提示信息是否正确
        上传文件，选择资源类型、教材、章节、知识点
        重名时使用推荐资源名
        """
        sleep(5)
        self.find_element(url.upload_resource).click()
        self.find_element(url.share_model(str(2))).click()
        self.find_element(url.confirm).click()
        file_warn = self.find_element(url.warn(str(1))).text
        resource_name_warn = self.find_element(url.warn(str(2))).text
        resource_type_warn = self.find_element(url.warn(str(3))).text
        flag = True
        if file_warn != '请上传资源文件！' or resource_name_warn != '请设置资源名称' or resource_type_warn != '请选择资源类型':
            return flag
        if self.isElementExist(url.warn(str(5))):
            book_warn = self.find_element(url.warn(str(5))).text
            if book_warn != '请选择教材':
                return flag
        if self.isElementExist(url.warn(str(6))):
            catalog_warn = self.find_element(url.warn(str(6))).text
            if catalog_warn != '请选择章节':
                return flag
        # 上传文件
        base_path = os.getcwd()
        resource_catalog = os.path.join(base_path, r'test_datas', r'resource_datas')
        file_list = os.listdir(resource_catalog)
        if len(file_list) == 1:
            file_name = file_list[0]
        else:
            file_name = file_list[random.randint(0, len(file_list) - 1)]
        self.upload_input(url.select_file, os.path.join(resource_catalog, file_name))
        sleep(2)
        if self.isElementExist(url.warn(str(1))):
            if file_warn == '请上传资源文件！':
                raise Exception('添加多媒体文件失败')
        else:
            self.isElementExist(url.annex_name)
            annex_name_text = self.find_element(url.annex_name).text
            # 选择资源类型
            self.log.info('上传了文件，附件名是' + annex_name_text + '')
            self.find_element(url.input_resource_type).click()
            resource_type_count = self.count_elements(url.resource_type_count)
            if resource_type_count == 1:
                self.log.info('有' + str(resource_type_count) + '个资源类型')
                self.find_element(url.resource_type(str(1))).click()
            else:
                type_index = str(random.randint(1, resource_type_count))
                self.find_element(url.resource_type(type_index)).click()
        self.find_element(url.share_model(str(1))).click()
        # 选择教材
        self.find_element(url.input_book).click()
        book_count = self.count_elements(url.book_count)
        if book_count == 0:
            raise Exception('无法选择教材')
        elif book_count == 1:
            self.log.info('有' + str(book_count) + '本教材')
            self.find_element(url.belong_book(str(1))).click()
        else:
            book_index = str(random.randint(1, book_count))
            self.find_element(url.belong_book(book_index)).click()
        # 选择章节
        sleep(2)
        self.find_element(url.input_catalog).click()
        catalog_count = self.count_elements(url.catalog_count)
        if catalog_count == 1:
            self.log.info('有' + str(catalog_count) + '个章节')
            self.find_element(url.belong_catalog(str(1))).click()
        else:
            catalog_index = str(random.randint(1, catalog_count))
            self.move_to(url.belong_catalog(catalog_index))
            self.find_element(url.belong_catalog(catalog_index)).click()
        # 选择知识点
        sleep(2)
        self.find_element(url.input_catalog).click()
        knowledge_count = self.count_elements(url.knowledge_count)
        if knowledge_count == 0:
            self.log.info('该章节下没有知识点')
        elif knowledge_count == 1:
            self.log.info('有一个知识点')
            self.find_element(url.belong_knowledge(str(1))).click()
        else:
            knowledge_index = str(random.randint(1, knowledge_count))
            self.find_element(url.belong_knowledge(knowledge_index)).click()
        self.find_element(url.confirm).click()
        alert = self.find_element(url.same_name_alert).text
        if alert == '已上传过同名资源，请为此资源设置新名称':
            if self.isElementExist(url.recommend_name):
                self.find_element(url.recommend_name).click()
            else:
                raise Exception('没有推荐名称')
            self.find_element(url.confirm).click()