# coding: utf-8
from time import sleep
from utils.UI.BasePage import BasePage
from page_locators.resource_locators.cloud_resource_locators import CloudResourceLocators as cloud
from page_objects.resource_page.check_course_page import CheckCoursePage
from page_objects.resource_page.cloud_resource_page import CloudResourcePage
from page_locators.resource_locators.upload_resource_locators import UploadResourceLocators as url
import random


class CloudCoursePage(BasePage):

    # 点击资源库导航进入资源库页面
    def to_resource_page(self):
        self.find_element(url.resource).click()

    def cloud_course(self):
        """
        资源库课程
        """
        sleep(5)
        self.find_element(cloud.resource_type(str(2))).click()
        course_count = self.count_elements(cloud.resource_card)
        index = str(random.randint(1, course_count - 1))
        self.log.info('生成的课程卡片随机数是' + index + '')
        # 进入课程详情
        self.move_to(cloud.resource_card_operate(index, str(1)))
        self.find_element(cloud.resource_card_operate(index, str(1))).click()
        course_name = self.find_element(cloud.course_name).text
        self.log.info('进入' + course_name + '课程详情页')

    def collect_course(self):
        """
        资源库收藏课程
        @return:
        """
        crp = CheckCoursePage(self.browser)
        cloud_resource = CloudResourcePage(self.browser)
        sleep(3)
        self.find_element(cloud.resource_type(str(2))).click()
        course_count = self.count_elements(cloud.resource_card)
        index = str(random.randint(1, course_count - 1))
        cloud_resource.move_collect(crp, index)

    def course_detail(self):
        """
        查看课程详情
        @return:
        """
        crp = CheckCoursePage(self.browser)
        self.to_resource_page()
        self.cloud_course()
        crp.resource_course_detail()

    def course_creator(self):
        """
        课程信息及创建人信息
        @return:
        """
        crp = CheckCoursePage(self.browser)
        self.to_resource_page()
        self.cloud_course()
        crp.course_creator_info()

    def history_version(self):
        """
        查看历史版本记录
        @return:
        """
        crp = CheckCoursePage(self.browser)
        self.to_resource_page()
        self.cloud_course()
        crp.history_version()

    def use_course(self):
        """
        引用课程
        @return:
        """
        crp = CheckCoursePage(self.browser)
        self.to_resource_page()
        self.cloud_course()
        crp.use_course()

    def check_recommend_course(self):
        """
        查看推荐课程
        @return:
        """
        crp = CheckCoursePage(self.browser)
        self.to_resource_page()
        self.cloud_course()
        crp.recommend_course()