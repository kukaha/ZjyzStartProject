# coding: utf-8
from time import sleep
from utils.UI.BasePage import BasePage
from page_locators.resource_locators.cloud_resource_locators import CloudResourceLocators as cloud
from page_locators.resource_locators.check_resource_locators import CheckResourceLocators as check
from page_locators.breadcrumb_locators import BreadcrumbLocators as bl
from page_objects.resource_page.check_course_page import CheckCoursePage
from page_objects.resource_page.check_resource_page import CheckResourcePage
from page_locators.resource_locators.upload_resource_locators import UploadResourceLocators as url
import random


class CloudResourcePage(BasePage):

    # 点击资源库导航进入资源库页面
    def to_resource_page(self):
        self.find_element(url.resource).click()
        sleep(3)

    def move_collect(self, crp, index):
        """
        鼠标移动到资源卡片收藏按钮
        收藏或取消收藏
        """
        self.move_to(cloud.resource_card_operate(index, str(3)))
        self.find_element(cloud.resource_card_operate(index, str(3))).click()
        operate_text = self.find_element(cloud.resource_operate_text(index, str(3))).text
        alert_text = self.find_element(cloud.collect_alert).text
        crp.collect(operate_text, alert_text)

    def collect_resource(self):
        """
        切换到微课资源：收藏/取消收藏资源
        """
        self.to_resource_page()
        crp = CheckCoursePage(self.browser)
        self.find_element(cloud.resource_type(str(3))).click()
        resource_card_count = self.count_elements(cloud.resource_card)
        index = str(random.randint(1, resource_card_count - 1))
        # 收藏资源
        self.move_collect(crp, index)

    def cloud_resource(self):
        """
        切换到微课，进入资源详情
        @return:
        """
        self.to_resource_page()
        self.find_element(cloud.resource_type(str(3))).click()
        resource_card_count = self.count_elements(cloud.resource_card)
        index = str(random.randint(1, resource_card_count - 1))
        self.move_to(cloud.resource_card_operate(index, str(1)))
        self.find_element(cloud.resource_card_operate(index, str(1))).click()
        self.isElementExist(bl.resource_info)
        resource_name = self.find_element(cloud.resource_name).text
        self.log.info('进入' + resource_name + '资源详情页')

    def cloud_add(self):
        """
        资源库云平台将资源添加到课程
        """
        self.to_resource_page()
        self.find_element(cloud.resource_type(str(3))).click()
        check_resource = CheckResourcePage(self.browser)
        resource_card_count = self.count_elements(cloud.resource_card)
        index = str(random.randint(1, resource_card_count - 1))
        self.move_to(cloud.resource_card_operate(index, str(2)))
        self.find_element(cloud.resource_card_operate(index, str(2))).click()
        add_to_course_count = self.count_elements(check.add_to_course_option_list)
        if add_to_course_count == 3:
            check_resource.add_teacher_plan()
        else:
            if check_resource.isElementExist(check.create_course):
                check_resource.add_course_ware()
            else:
                check_resource.add_resource()
        self.back_page()

    def resource_detail_add(self):
        """
        资源详情页添加到课程按钮
        """
        self.cloud_resource()
        check_resource = CheckResourcePage(self.browser)
        check_resource.add_resource_button()
        self.back_page()
        self.find_element(cloud.resource_back).click()

    def resource_detail_collect(self):
        """
        资源详情页，点击收藏按钮
        @return:
        """
        self.cloud_resource()
        check_resource = CheckResourcePage(self.browser)
        check_resource.resource_collect()

    def resource_qr_code(self):
        """
        资源详情页，点击资源二维码
        @return:
        """
        self.cloud_resource()
        check_resource = CheckResourcePage(self.browser)
        check_resource.resource_qr_code()

    def resource_error_recovery(self):
        """
        资源详情页，纠错
        @return:
        """
        self.cloud_resource()
        check_resource = CheckResourcePage(self.browser)
        check_resource.resource_correct_error()

    def resource_recommend(self):
        """
        资源详情页，推荐资源
        @return:
        """
        self.cloud_resource()
        check_resource = CheckResourcePage(self.browser)
        check_resource.recommend_resource()