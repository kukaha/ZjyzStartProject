# coding: utf-8
from time import sleep
from utils.UI.BasePage import BasePage
from page_locators.resource_locators.check_course_locators import CheckCourseLocators as ccrl
import random


class CheckCoursePage(BasePage):

    def accredit(self):
        """
        查看课程中资源和教案是否有授权
        if有教案时，教案是否有授权
        else资源是否有授权
        """
        # 教案是否有授权
        if self.isElementExist(ccrl.teaching_plan):
            text = self.find_element(ccrl.add_teaching_plan_button).text
            if text == '无授权':
                self.log.info('该教案没有授权')
                self.move_to(ccrl.add_teaching_plan_button)
                self.isElementExist(ccrl.accredit_hover)
                self.find_element(ccrl.add_teaching_plan_button).click()
                self.isElementExist(ccrl.accredit_alert)
            else:
                self.add_resource(ccrl.add_teaching_plan_button)
        # 资源是否有授权
        elif self.isElementExist(ccrl.add_resource_button(str(1))):
            count = self.count_elements(ccrl.add_resource_list)
            course_resource_index = str(random.randint(1, count))
            if count == 0:
                self.log.info('该环节下没有资源')
            self.log.info('第' + course_resource_index + '个资源')
            text = self.find_element(ccrl.add_resource_button(course_resource_index)).text
            self.resource_author(course_resource_index, text)

    def resource_author(self, course_resource_index, text):
        """
        查看资源是否授权，如果有授权的话添加到课程
        :param course_resource_index: 选择第几个资源
        :param text: 选择资源的文本
        """
        if text == '无授权':
            self.log.info('该资源未授权')
            self.move_to(ccrl.add_resource_button(course_resource_index))
            self.isElementExist(ccrl.accredit_hover)
            self.find_element(ccrl.add_resource_button(course_resource_index)).click()
            self.isElementExist(ccrl.accredit_alert)
        else:
            self.add_resource(ccrl.add_resource_button(course_resource_index))

    def add_resource(self, loc):
        """
        将课程中的资源/教案/课程添加到其他课程中
        :param loc:
        """
        self.find_element(loc).click()
        # self.find_element(ccrl.add_resource_to_class(str(1))).click()
        # self.find_element(ccrl.first_book).click()
        self.find_element(ccrl.add_resource_to_class(str(2))).click()
        self.find_element(ccrl.first_catalog).click()
        self.find_element(ccrl.add_resource_to_class(str(3))).click()
        course_count = self.count_elements(ccrl.course_list)
        course_index = str(random.randint(1, course_count))
        if course_count > 1:
            self.execute_script(ccrl.select_course_name(course_index))
            sleep(2)
            self.log.info('选择第' + course_index + '个课程')
            self.find_element(ccrl.select_course_name(course_index)).click()
        if self.isElementExist(ccrl.add_resource_to_class(str(4))):
            if self.isElementExist(ccrl.creat_course):
                self.log.info('添加课件到课程')
            else:
                self.log.info('添加资源到课程')
            self.find_element(ccrl.add_resource_to_class(str(4))).click()
            step_count = self.count_elements(ccrl.step_list)
            step_index = str(random.randint(1, step_count))
            if step_count > 1:
                self.find_element(ccrl.course_step(step_index)).click()
            else:
                self.find_element(ccrl.course_step(str(1))).click()
        else:
            self.log.info('添加教案到课程')
        sleep(2)
        self.find_element(ccrl.confirm).click()
        added_alert = self.find_element(ccrl.alert).text
        if added_alert == '当前环节中已存在该资源！':
            self.find_element(ccrl.cancel).click()

    def collect(self, button_text, alert_text):
        """
        收藏功能
        收藏-收藏成功
        已收藏-取消收藏
        """
        if (button_text == '收 藏' or button_text == '收藏') and alert_text == '收藏成功':
            self.log.info('收藏成功')
        elif button_text == '已收藏' and alert_text == '取消收藏成功':
            self.log.info('取消收藏成功')
        else:
            raise Exception('收藏或取消收藏失败')

    def resource_course_detail(self):
        """
        资源库课程详情页：
        课程中有教案时，查看教案，添加到课程
        课程中有资源时，查看资源，预览资源，添加到课程
        """
        # 课程中有教案时，查看教案，添加到课程
        if self.isElementExist(ccrl.teaching_plan):
            self.log.info('该课程有教案')
            self.find_element(ccrl.teaching_plan).click()
            button_text = self.find_element(ccrl.resource_detail(str(1))).text
            self.find_element(ccrl.resource_detail(str(1))).click()
            alert_text = self.find_element(ccrl.alert).text
            self.collect(alert_text, button_text)
            sleep(3)
            self.find_element(ccrl.teaching_plan).click()
            self.add_resource(ccrl.resource_detail(str(2)))
        # 查看资源
        resource_count = self.count_elements(ccrl.course_resource_list)
        resource_index = str(random.randint(1, resource_count))
        if resource_count == 0:
            self.log.info('该环节下没有资源')
        elif resource_count == 1:
            self.find_element(ccrl.course_resource(str(1))).click()
        else:
            self.find_element(ccrl.course_resource(resource_index)).click()
            if self.isElementExist(ccrl.close):
                self.find_element(ccrl.close).click()
            else:
                raise Exception('预览资源失败')
            # 查看资源授权
        self.find_element(ccrl.step_header(str(2))).click()
        self.accredit()
        step_count = self.count_elements(ccrl.step_header_list)
        if step_count == 2:
            self.log.info('查看的课程是自定义课程')
        else:
            self.log.info('查看的课程是翻转课堂')
            self.find_element(ccrl.step_header(str(3))).click()
            self.accredit()

    def course_creator_info(self):
        """
        课程相关信息和课程创建人信息
        """
        total_amount = self.find_element(ccrl.courses_count(str(1))).text
        total_collect = self.find_element(ccrl.courses_count(str(2))).text
        share_date = self.find_element(ccrl.course_info(str(1))).text
        belong_book = self.find_element(ccrl.course_info(str(2))).text
        belong_catalog = self.find_element(ccrl.course_info(str(3))).text
        creator = self.find_element(ccrl.course_creator(str(1))).text
        creator_unit = self.find_element(ccrl.course_creator(str(2))).text
        sleep(3)
        self.log.info('总使用量：' + total_amount + ' 总收藏量：' + total_collect + ' ' + share_date + ' ' + belong_book + ' ' + belong_catalog + ' 创建人：' + creator + ' 所属学校：' + creator_unit)

    def history_version(self):
        """
        查看课程历史版本记录
        课程名称一致时
        return True
        """
        sleep(1)
        self.find_element(ccrl.share_history).click()
        version_text = self.find_element(ccrl.version_text).text
        self.find_element(ccrl.share_history_version).click()
        course_version_text = self.find_element(ccrl.course_version_text).text
        if version_text == course_version_text:
            return True
        else:
            raise Exception('切换版本失败')

    def recommend_course(self):
        """
        查看推荐课程，进入课程详情页,课程名称一致时
        :return True
        """
        recommend_course_count = self.count_elements(ccrl.recommend_course_list)
        course_index = str(random.randint(2, recommend_course_count + 1))
        sleep(1)
        course_text = self.find_element(ccrl.recommend_course(course_index)).text
        print(course_text)
        self.find_element(ccrl.recommend_course(course_index)).click()
        sleep(1)
        current_course_text = self.find_element(ccrl.course_name).text
        print(current_course_text)
        if course_text == current_course_text:
            self.log.info('进入推荐课程' + current_course_text + '课程详情页')
            return True
        else:
            raise Exception('切换推荐课程失败')

    def use_course(self):
        """
        引用课程：如果存在未授权资源，弹框提示未授权的资源
        """
        if self.isElementExist(ccrl.alert):
            self.log.info('资源添加成功的提示还存在')
            sleep(3)
            self.find_element(ccrl.use_course_button).click()
        else:
            self.find_element(ccrl.use_course_button).click()
            if self.isElementExist(ccrl.use_course_warn):
                use_course_warn = self.find_element(ccrl.use_course_warn).text
                no_author_resource = self.find_element(ccrl.no_author_resource).text
                self.log.info(use_course_warn + ':' + no_author_resource)
                self.find_element(ccrl.confirm1).click()
        self.find_element(ccrl.confirm).click()
        use_success_alert = self.find_element(ccrl.alert).text
        if use_success_alert == '引用课程成功':
            self.log.info('课程引用成功')
        else:
            raise Exception('课程引用失败')
