# coding: utf-8
from time import sleep
from utils.UI.BasePage import BasePage
from page_locators.resource_locators.check_combine_resource_locators import CheckCombineResourceLocators as combineresource
from page_locators.resource_locators.cloud_resource_locators import CloudResourceLocators as cloudresource
import random


class CheckCourseResourcePage(BasePage):

    def check_resource(self):
        """
        切换至专题资源，查看组合资源
        """
        self.find_element(combineresource.topic_tab).click()
        sleep(1)
        self.find_element(combineresource.resource_type).click()
        sleep(1)
        self.move_to(cloudresource.resource_card_operate(str(1), str(1)))
        self.find_element(cloudresource.resource_card_operate(str(1), str(1))).click()
        resource_count = self.count_elements(combineresource.resource_list)
        index = str(random.randint(2, resource_count))
        self.find_element(combineresource.resource_name(index)).click()
        sleep(1)
        # 视频在iframe中，切换至iframe，播放视频
        self.switch_to_frame(combineresource.iframe_loc)
        if self.isElementExist(combineresource.play_btn):
            self.find_element(combineresource.play_btn).click()
            sleep(2)
            play_class = self.find_element(combineresource.player).get_attribute('class')
            if play_class == 'pv-video-player pv-skin_blue-15919310 pv-skin_blue-player pv-full-wrap':
                self.log.info('95教研室视频已播放')
        elif self.isElementExist(combineresource.play_btn2):
            self.find_element(combineresource.play_btn2).click()
            sleep(2)
            play_class = self.find_element(combineresource.player).get_attribute('class')
            if play_class.contains('vjs-playing'):
                self.log.info('博雅云课堂视频已播放')
        elif self.isElementExist(combineresource.content_text):
            self.log.info('查看文本，已显示内容')
        elif self.isElementExist(combineresource.content_img):
            self.log.info('查看图片，已显示图片')
        else:
            raise Exception('视频播放失败')
