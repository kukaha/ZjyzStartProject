# coding: utf-8
from time import sleep
from utils.UI.BasePage import BasePage
from page_locators.resource_locators.check_resource_locators import CheckResourceLocators as crl
from page_locators.resource_locators.cloud_resource_locators import CloudResourceLocators as cloud
from page_locators.resource_locators.check_course_locators import CheckCourseLocators as ccrl
from page_objects.resource_page.check_course_page import CheckCoursePage
from page_locators.breadcrumb_locators import BreadcrumbLocators as bl
import random


class CheckResourcePage(BasePage):

    def add_resource_button(self):
        """
        资源详情页添加到课程按钮：教案/课件/资源
        """
        self.find_element(crl.buttons(str(1))).click()
        add_to_course_count = self.count_elements(crl.add_to_course_option_list)
        if add_to_course_count == 3:
            self.add_teacher_plan()
        else:
            if self.isElementExist(crl.create_course):
                self.add_course_ware()
            else:
                self.add_resource()

    def resource_collect(self):
        """
        资源详情页收藏
        @return:
        """
        check_course = CheckCoursePage(self.browser)
        self.find_element(crl.buttons(str(2))).click()
        button_text = self.find_element(crl.collect_button_text).text
        alert_text = self.find_element(cloud.collect_alert).text
        check_course.collect(button_text, alert_text)

    def resource_qr_code(self):
        """
        资源详情页资源二维码
        @return:
        """
        self.find_element(crl.buttons(str(3))).click()
        if self.isElementExist(crl.qr_code_img):
            self.log.info('打开了资源二维码')
            self.find_element(crl.close).click()
        else:
            raise Exception('资源二维码打开失败')

    def resource_correct_error(self):
        """
        资源详情页纠错
        @return:
        """
        self.find_element(crl.buttons(str(4))).click()
        self.find_element(crl.error_type).click()
        self.find_element(crl.error_description).send_keys('测试纠错描述')
        self.find_element(crl.confirm1).click()

    def recommend_resource(self):
        """
        查看推荐资源
        :return True
        """
        recommend_resource_count = self.count_elements(crl.recommend_resource_list)
        resource_index = str(random.randint(1, recommend_resource_count))
        sleep(2)
        resource_text = self.find_element(crl.recommend_resource(resource_index)).text
        self.find_element(crl.recommend_resource(resource_index)).click()
        current_resource_text = self.find_element(crl.resource_name).text
        if resource_text == current_resource_text:
            self.log.info('进入推荐资源' + current_resource_text + '资源详情页')
            self.find_element(crl.close).click()
            return True
        else:
            raise Exception('切换推荐资源失败')

    def select_step(self):
        """
        添加资源时选择课程环节
        """
        if self.isElementExist(crl.step_count):
            step_count = self.count_elements(crl.step_count)
            index = str(random.randint(1, step_count))
            if step_count == 1:
                self.find_element(crl.step(str(1))).click()
            elif step_count > 1:
                self.find_element(crl.step(index)).click()

    def add_confirm(self):
        """
        添加资源时点确定，添加资源
        """
        self.find_element(crl.confirm).click()
        if self.isElementExist(crl.skip_link_to_course):
            self.find_element(crl.skip_link_to_course).click()
            self.isElementExist(bl.class_info)
            self.log.info('进入了课程详情页')
        elif self.isElementExist(cloud.added_alert):
            self.log.info('已添加该资源')
        else:
            raise Exception('添加失败')

    def add_resource(self):
        """
        资源详情中，添加资源
        """
        try:
            self.find_element(crl.add_to_select_course).click()
            course_count = self.count_elements(ccrl.course_list)
            course_index = str(random.randint(1, course_count))
            if course_count >= 1:
                self.find_element(ccrl.select_course_name(course_index)).click()
            self.find_element(crl.add_to_select_step).click()
            self.select_step()
            self.add_confirm()
        except:
            raise Exception('添加资源到课程失败')

    def add_course_ware(self):
        """
        资源详情中添加课件
        """
        try:
            self.find_element(crl.confirm).click()
            if self.isElementExist(crl.course_name_warn):
                self.find_element(crl.create_course).click()
                self.find_element(crl.confirm1).click()
                self.find_element(crl.add_to_select_step).click()
            else:
                self.find_element(crl.add_to_select_step).click()
            self.select_step()
            self.add_confirm()
        except:
            raise Exception('添加课件到课程失败')

    def add_teacher_plan(self):
        """
        资源详情中添加教案，添加成功后点击课程链接进入课程详情页
        """
        try:
            self.find_element(crl.confirm).click()
            if self.isElementExist(crl.course_name_warn):
                self.find_element(crl.create_course).click()
                self.find_element(crl.confirm1).click()
                self.find_element(crl.confirm).click()
                self.find_element(crl.skip_link_to_course).click()
                if self.isElementExist(bl.class_info):
                    self.log.info('进入了课程详情页')
                else:
                    raise Exception('进入课程详情失败')
        except:
            raise Exception('添加教案到课程失败')

