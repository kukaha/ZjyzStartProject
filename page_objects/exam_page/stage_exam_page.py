# coding: utf-8
from time import sleep
from utils.UI.BasePage import BasePage
from page_locators.exam_locators.stage_exam_locators import StageExamLocators as stageel
from page_locators.breadcrumb_locators import BreadcrumbLocators as bl
from page_objects.exam_page.exam_info_page import ExamInfoPage
import random


class StageExamPage(BasePage):

    def to_exam_page(self):
        sleep(2)
        self.find_element(stageel.exam).click()

    def check_exam(self):
        """
        点击进入试卷详情
        """
        exam_count = self.count_elements(stageel.exam_count)
        exam_index = str(random.randint(1, exam_count))
        exam_text = self.find_element(stageel.exam_list(exam_index)).text
        self.find_element(stageel.exam_list(exam_index)).click()
        title_text = self.find_element(stageel.exam_info_title).text
        if exam_text == title_text:
            self.log.info('进入' + title_text + '试卷详情页')
        else:
            raise Exception('查看试卷失败')

    def print_exam(self):
        """
        进入打印试卷页面
        """
        exam_count = self.count_elements(stageel.exam_count)
        exam_index = str(random.randint(1, exam_count))
        self.find_element(stageel.exam_operate(exam_index, str(2))).click()
        sleep(3)
        if self.isElementExist(stageel.cancel):
            self.find_element(stageel.cancel).click()

    def add_exam_to_course(self):
        """
        添加试卷到课程，跳转到课程详情
        """
        exam_count = self.count_elements(stageel.exam_count)
        exam_index = str(random.randint(1, exam_count))
        self.find_element(stageel.exam_operate(exam_index, str(3))).click()
        sleep(2)
        # 选择课程
        self.find_element(stageel.my_course).click()
        self.find_element(stageel.first_course).click()
        # 选择环节
        self.find_element(stageel.teaching_step_div).click()
        if self.isElementExist(stageel.teaching_step_count):
            index = str(random.randint(1, 3))
            self.find_element(stageel.teaching_step(index)).click()
        else:
            self.log.info('课程是自定义课程，只有一个默认环节')
        self.find_element(stageel.confirm).click()
        sleep(0.1)
        if self.isElementExist(stageel.skip_course):
            skip_course_text = self.find_element(stageel.skip_course).text
            self.find_element(stageel.skip_course).click()
            course_text = self.find_element(stageel.course_name).text
            if skip_course_text == course_text:
                self.log.info('试卷已添加到' + course_text + '')
            else:
                self.log.info('课程链接名：' + skip_course_text + '课程名：' + course_text + '')
        else:
            self.log.info('已添加该资源')
            self.find_element(stageel.back).click()
        # elif self.isElementExist(stageel.alert):
        #     if self.find_element(stageel.alert).text == '当前环节中已存在该资源！':
        #         self.log.info('已添加该资源')
        #         self.find_element(stageel.back).click()
        # else:
        #     raise Exception('添加试卷失败')

    def collect_exam(self):
        """
        试卷收藏功能
        @return:
        """
        exam_count = self.count_elements(stageel.exam_count)
        exam_index = str(random.randint(1, exam_count))
        button_text = self.find_element(stageel.exam_operate(exam_index, str(4))).text
        self.find_element(stageel.exam_operate(exam_index, str(4))).click()
        alert_text = self.find_element(stageel.alert).text
        if button_text == '添加收藏' and alert_text == '收藏成功':
            self.log.info('收藏成功')
        elif button_text == '已收藏' and alert_text == '取消收藏成功':
            self.log.info('取消收藏成功')
        else:
            raise Exception('收藏或取消收藏失败')

    def assign_homework(self):
        """
        布置作业
        @return:
        """
        exam_count = self.count_elements(stageel.exam_count)
        exam_index = str(random.randint(1, exam_count))
        self.find_element(stageel.exam_operate(exam_index, str(1))).click()
        self.isElementExist(bl.assign_homework)
        self.find_element(bl.exam_a).click()

    def info_test_detail(self):
        exam_info = ExamInfoPage(self.browser)
        self.check_exam()
        exam_info.test_detail()

    def info_answer_on_off(self):
        exam_info = ExamInfoPage(self.browser)
        self.check_exam()
        exam_info.answer_on_off()

    def info_copy_exam(self):
        exam_info = ExamInfoPage(self.browser)
        self.check_exam()
        exam_info.copy_exam()

    def info_add_to_course(self):
        exam_info = ExamInfoPage(self.browser)
        self.check_exam()
        exam_info.add_to_course()