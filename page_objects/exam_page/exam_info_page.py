# coding: utf-8
import random
from time import sleep

from utils.UI.BasePage import BasePage
from page_locators.exam_locators.exam_info_locators import ExamInfoLocators as examl
from page_locators.breadcrumb_locators import BreadcrumbLocators as bl


class ExamInfoPage(BasePage):

    def test_detail(self):
        """
        试卷详情页，试题详情
        @return:
        """
        major_test_list = self.find_elements(examl.major_test_count)
        major_test_count = self.count_elements(examl.major_test_count)
        self.log.info('共' + str(major_test_count) + '个大题')
        for major_test in range(0, major_test_list.__len__()):
            index = str(major_test + 1)
            test_form_text = self.find_element(examl.test_form(index)).text
            test_count_text = self.find_element(examl.test_count_text(index)).text
            count = test_count_text[3: -3]
            test_count = self.count_elements(examl.test_count(index))
            if count == str(test_count):
                self.log.info('第' + index + '个题为：' + test_form_text + ',' + test_count_text)

    def answer_on_off(self):
        """
        显示答案开关
        """
        self.find_element(examl.exam_info_button(str(1))).click()
        if self.isElementExist(examl.choice_question_answer) or self.isElementExist(examl.gap_answer):
            return True
        else:
            self.log.info('没有显示正确答案')

    def copy_exam(self):
        """
        复制试卷，进入组卷页面
        """
        self.find_element(examl.exam_info_button(str(2))).click()
        copy_warn_text = self.find_element(examl.copy_warn).text
        self.log.info('警告:' + copy_warn_text)
        self.find_element(examl.confirm).click()
        alert_text = self.find_element(examl.alert).text
        if alert_text == '复制试卷成功正在跳转...':
            if self.isElementExist(bl.composition_test):
                return True

    def add_to_course(self):
        """
        添加试卷到课程中，添加成功后跳转到课程详情页
        """
        self.find_element(examl.exam_info_button(str(3))).click()
        # 选择课程
        self.find_element(examl.my_course).click()
        self.find_element(examl.first_course).click()
        # 选择环节
        self.find_element(examl.teaching_step_div).click()
        if self.isElementExist(examl.teaching_step_count):
            index = str(random.randint(1, 3))
            self.find_element(examl.teaching_step(index)).click()
        else:
            self.log.info('课程是自定义课程，只有一个默认环节')
        self.find_element(examl.confirm2).click()
        # 添加成功进入课程详情
        sleep(0.1)
        if self.isElementExist(examl.skip_link):
            self.find_element(examl.skip_link).click()
            if self.isElementExist(bl.class_info):
                course_name = self.find_element(examl.course_title).text
                self.log.info('进入课程详情' + course_name + '课程详情页')
        else:
            self.log.info('已添加该资源')
            self.find_element(examl.back).click()