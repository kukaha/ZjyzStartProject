# coding: utf-8
import time
from time import sleep

from utils.UI.BasePage import BasePage
from page_locators.exam_locators.generate_exam_locators import GenerateExamLocators as generate


class GenerateExamPage(BasePage):

    def to_catalog_exam(self):
        """
        智能组卷：按章节组卷
        """
        sleep(3)
        self.find_element(generate.generate_exam(str(2))).click()
        sleep(2)
        self.move_to(generate.drop_down(str(1)))
        self.find_element(generate.drop_down(str(1))).click()
        self.move_to(generate.accurate_tip_img)
        self.isElementExist(generate.tip_inner(str(3)))
        self.move_to(generate.simple_tip_img)
        self.isElementExist(generate.tip_inner(str(4)))
        self.find_element(generate.select_catalog(str(1))).click()

    def to_knowledge_exam(self):
        """
        智能组卷：按知识点组卷
        """
        sleep(3)
        self.find_element(generate.generate_exam(str(2))).click()
        sleep(2)
        self.move_to(generate.drop_down(str(2)))
        self.find_element(generate.drop_down(str(2))).click()
        knowledge = self.find_element(generate.knowledge_span1)
        # 展开并选择第一个知识点及子节点
        while True:
            unfold_span = knowledge.find_element_by_xpath("./span[2]")
            checkbox_span = knowledge.find_element_by_xpath("./span[3]")
            checkbox_span.click()
            unfold_span_class = unfold_span.get_attribute('class')
            if unfold_span_class == 'ant-tree-switcher ant-tree-switcher_close':
                unfold_span.click()
                sleep(1)
                knowledge = knowledge.find_element_by_xpath("./following-sibling::div[1]")
            else:
                break

    def select_test_form(self):
        """
        选择题型
        """
        sleep(3)
        test_form_list = self.find_elements(generate.test_form_list)
        test_list = []
        # 选择题型
        for test_form in range(1, test_form_list.__len__() + 1):
            test_text = self.find_element(generate.test_form(str(test_form))).text
            test_count_text = int(test_text[test_text.index("(") + 1: -2])
            test_form_text = test_text[0:test_text.index("(")-2]
            if test_count_text == 0:
                self.log.info(test_form_text + '总题数为0')
            else:
                self.find_element(generate.test_form(str(test_form))).click()
                test_list.append(test_form_text)
        return test_list

    def cross_set_input(self):
        """
        交叉设置，输入题型题量
        """
        test_form_count = self.count_elements(generate.test_form_check)
        test_form_input = self.find_elements(generate.cross_set_count_input)
        # 设置每个题型总量
        if test_form_count == test_form_input.__len__() - 1:
            for input_index in range(1, test_form_input.__len__()):
                self.find_element(generate.cross_set_input(str(input_index))).send_keys(5)
            if self.isElementExist(generate.test_form_count_few):
                self.log.info('试题不足，已自动调整')
        self.find_element(generate.generate_exam_btn).click()

    def free_set_input(self):
        """
        自由设置，输入题型题量
        """
        self.find_element(generate.free_set_way).click()
        test_form_count = self.count_elements(generate.test_form_check)
        test_form_input = self.find_elements(generate.free_set_count_input)
        # 设置每个题型总量
        if test_form_count == test_form_input.__len__():
            for input_index in range(1, test_form_input.__len__() + 1):
                self.find_element(generate.free_set_input(str(input_index))).send_keys(5)
            if self.isElementExist(generate.test_form_count_few):
                self.log.info('试题不足，已自动调整')
        self.find_element(generate.generate_exam_btn).click()

    def exam_detail(self, test_count):
        """
        试卷详情页大题总数
        @param: 选择的题型总数
        """
        detail_test_count = self.count_elements(generate.detail_test_count)
        if test_count.__len__() != detail_test_count:
            self.log.info('生成的试题总数不一致')
            raise Exception('生成试卷失败')
        else:
            self.log.info('一共生成了' + str(detail_test_count) + '个大题')
        self.find_element(generate.adjust_exam_btn).click()
        detail_test_list = self.find_elements(generate.detail_select_test_form)
        select_text = []
        for detail_test in detail_test_list:
            detail_test_text = detail_test.text
            test_form_text = detail_test_text[0:detail_test_text.index("(")-2]
            select_text.append(test_form_text)
        if test_count == select_text:
            return True

    def complete_exam(self):
        """
        点击完成组卷，没有试卷名称或试卷名称重复时重新输入试卷名
        """
        self.find_element(generate.complete_exam_btn).click()
        tips = self.find_element(generate.tips).text
        self.log.info(tips)
        self.find_element(generate.confirm).click()
        if self.isElementExist(generate.alert):
            while True:
                sleep(0.2)
                alert_text = self.find_element(generate.alert).text
                if alert_text != '请稍后...':
                    break
            if alert_text == '提交试卷失败：试卷标题不能为空' or alert_text == '提交试卷失败：试卷名称重复，请重新设置':
                self.find_element(generate.edit_exam_name).click()
                self.find_element(generate.input_exam_name).send_keys('测试试卷' + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
                self.find_element(generate.complete_exam_btn).click()
                tips = self.find_element(generate.tips).text
                self.log.info(tips)
                self.find_element(generate.confirm).click()
        if self.find_element(generate.my_exam_tab).get_attribute('aria-selected') == 'true':
            self.log.info('进入我的试卷页')
            return True

    def generate_knowledge_exam(self):
        """
        默认出卷方式和题量生成试卷
        """
        self.to_knowledge_exam()
        test_count = self.select_test_form()
        self.cross_set_input()
        self.exam_detail(test_count)
        self.complete_exam()

    def generate_catalog_exam(self):
        """
        自由设置题量生成试卷
        """
        self.to_catalog_exam()
        test_count = self.select_test_form()
        self.free_set_input()
        self.exam_detail(test_count)
        self.complete_exam()
