# coding=utf-8
# @time   :2021/08/26
# @Author :wushaotang
# @File   : errors_set_page.py
# @Description: 中教云智慧教学平台-学生错题集
# @Return  :


import re
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from utils.UI.BasePage import BasePage
from page_locators.errors_set_locators import ErrorsLocators as el

class ErrorsSerPage(BasePage):
    def __init__(self, driver: webdriver.Remote):
        super().__init__(driver)

    def my_errors(self):
        '''
        初始化菜单错题集页面
        '''
        self.find_element(el.menu_errors).click()

    def is_left_menu(self):
        '''
        对该页面元素的验证
        '''
        assert self.find_element(el.left_student).text == '学生'
        assert self.find_element(el.left_section).text == '章节'
        assert self.find_element(el.left_point).text == '知识点'