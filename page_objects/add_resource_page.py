# coding=utf-8
# @time   :2021/08/13
# @Author :wushaotang
# @File   : add_resource_page.py
# @Description: 中教云智慧教学平台-备授课-添加资源
# @Return  :

from selenium.webdriver.common.by import By
import re
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from utils.UI.BasePage import BasePage
from page_locators.add_resource_locators import AddResourceLocators as al



class AddResourcePage(BasePage):
    def __init__(self, driver: webdriver.Remote):
        super().__init__(driver)
        self.resource_name = 0
        self.job_name = 0

    def add_resource(self):
        '''
        给课程添加资源
        '''
        type_list = []
        for course in al.course_type_list():
            if not self.isElementExist(course):
                self.log.info('当前没有第{}课程，不能添加资源'.format(al.course_type_list().index(course) + 1))
                return
            else:
                type = self.find_element(course).text
                if type not in type_list:
                    type_list.append(type)
                    index = al.course_type_list().index(course)     #获取当前课程的下标
                    self.judeg_type(type, index)    #判断课程类型并添加资源
                    self.back_page()                #回到课程列表页



    def judeg_type(self,element, index):
        '''

        '''
        if element == '翻转课堂':
            "调用翻转课堂的方法"
            try:
                self.turn_add(index)
                self.tes_judeg()        #资源添加成功
            except Exception as e:
                self.log.info('翻转课堂添加资源失败')
                self.get_screenshot_name('翻转课堂添加资源失败')
            try:
                self.job_add_turn()             #添加作业
                self.tes_judeg_job()    #作业添加成功
            except Exception as e:
                self.log.info('翻转课堂添加作业失败:{}'.format(e))
                self.get_screenshot_name('翻转课堂添加作业失败')

        elif element == '自定义':
            "调用自定义课程的方法"
            try:
                self.custom_add(index)
                self.tes_judeg()
                # raise Exception('------------------')
            except Exception as e:
                self.log.info('自定义课堂添加资源失败')
                self.get_screenshot_name('自定义课堂添加资源失败')
            try:
                self.job_add_custom()
                self.tes_judeg_job()
                self.back_page()  # 回上一页：
            except Exception as e:
                self.log.info('自定义课堂添加作业失败:{}'.format(e))
                self.get_screenshot_name('自定义课堂添加作业失败')


    def turn_add(self, index):
        '''
        翻转课堂添加资源
        '''
        self.find_element(al.course_location()[index]).click()     #点击该课程名，跳转进入详情页
        time.sleep(2)
        self.find_element(al.add_resource).click()
        self.find_element(al.resource_warehouse).click()        #点击资源库，跳转资源库的弹窗
        for i in range(1,9):
            if self.find_element(al.if_add_resource_location(i)).get_attribute('class') == 'img_five':    #尚未被添加
                self.find_element(al.if_add_resource_location(i)).click()
                self.resource_name = self.find_element(al.add_resource_name(i)).text
                self.log.info('翻转-课堂添加资源名为‘《{}》’'.format(self.resource_name))
                self.find_element(al.resource_submit).click()
                break

    def custom_add(self, index):
        '''
        自定义 添加资源
        '''
        self.find_element(al.course_location()[index]).click()      #点击该课程名，跳转进入详情页
        time.sleep(1)
        self.find_element(al.custom_add).click()
        self.find_element(al.resource_warehouse).click()        #点击资源库，跳转资源库的弹窗
        for i in range(1,9):
            if self.find_element(al.if_add_resource_location(i)).get_attribute('class') == 'img_five':    #尚未被添加
                self.find_element(al.if_add_resource_location(i)).click()
                self.resource_name = self.find_element(al.add_resource_name(i)).text
                self.log.info('自定义-课堂添加资源名为‘《{}》’'.format(self.resource_name))
                self.find_element(al.resource_submit).click()
                break

    def tes_judeg(self):
        '''
        判断资源是否添加成功
        '''
        assert self.wait_text(self.resource_name)
        self.log.info('翻转课程-有资源 {}'.format(self.resource_name))


    def job_add_turn(self):
        '''
        翻转--添加作业
        '''
        time.sleep(1)
        self.find_element(al.add_job).click()
        # if not self.isElementExist(al.resource_1):      #判断课前是否有资源可以供添加
        #     self.log.info('翻转课程内没有资源可供布置作业')
        #     return
        # else:
        # self.browser.find_element_by_xpath('//div/span[contains(text(),"+ 添加全部")]').click()
        self.find_element(al.add_all).click()       #点击添加所有资源
        self.find_element(al.next_button).click()
        time.sleep(1)
        self.job_name = self.find_element(al.job_name).get_attribute('value')       #获取默认作业名并写入公共变量
        self.log.info('翻转课程添加的作业名为《{}》'.format(self.job_name))
        self.find_element(al.job_time).click()
        self.find_element(al.job_today).click()
        # self.find_element(al.job_mode).click()
        time.sleep(2)
        self.find_element(al.job_submit).click()

    def job_add_custom(self):
        '''
        自定义--布置作业
        '''
        self.find_element(al.add_job_custom).click()
        if not self.isElementExist(al.resource_custom_1):      #判断课前是否有资源可以供添加
            self.log.info('翻转课程内没有资源可供布置作业')
            return
        else:
            time.sleep(1)
            self.find_element(al.add_all).click()       #点击添加所有资源
            self.find_element(al.next_button).click()
            time.sleep(1)
            self.job_name = self.find_element(al.job_name).get_attribute('value')  # 获取默认作业名并写入公共变量
            self.log.info('自定义-课程添加的作业名为《{}》'.format(self.job_name))
            self.find_element(al.job_time).click()
            self.find_element(al.job_today).click()
            # self.find_element(al.job_mode).click()
            time.sleep(2)
            self.find_element(al.job_submit).click()
            time.sleep(1)
            self.find_element(al.job_page).click()

    def tes_judeg_job(self):
        '''
        判断作业是否添加成功
        '''
        time.sleep(2)
        assert self.wait_text(self.job_name)
        self.log.info('作业 {} 添加成功'.format(self.job_name))





