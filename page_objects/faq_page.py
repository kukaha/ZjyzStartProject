import time
from selenium.webdriver.common.by import By

from selenium import webdriver

from utils.UI.BasePage import BasePage
from page_locators.faq_locators import FAQLocators as fl

class FAQPage(BasePage):

    def __init__(self, driver: webdriver.Remote):
        super().__init__(driver)
        self._count = 0

    def faq(self):
        # 点击帮助中心
        self.find_element(fl.menu_faq).click()
        # 获得左侧菜单项的数量
        menu_count = self.get_ele_count(fl.faq_menulist)
        self.log.info('共有{}个菜单项'.format(menu_count))
        # 菜单项的下标值
        index = 1
        # 通过循环来遍历左侧的菜单项
        while index < menu_count+1:
            # 通过方法获取每一个菜单项的位置
            menu_byid = fl.find_menu_byid(str(index))
            self.find_element(menu_byid).click()
            index += 1
            # 在该菜单页停留2秒
            video = fl.video
            # 判断该页面是否有视频
            if self.isElementExist(video):
                time.sleep(2)
                # 获得每个页面的右侧列表视频数
                videos_count = self.get_ele_count(fl.videos_count)
                self.log.info("该页面有{}个视频".format(videos_count))
                index1 = 1
                # 通过循环来判断每一个视频的已看时长是不是小于总时长
                while index1 <= videos_count:
                    # 获取每个视频的已看时长
                    watchedtime = fl.get_video_watchedtime(str(index1))
                    viedo_watchedtime = self.find_element(watchedtime).text[3:]
                    self.log.info("第{}个视频已看时长为{}".format(index1,viedo_watchedtime))
                    # 获取每个是的总时长
                    alltime = fl.get_video_alltime(str(index1))
                    video_alltime = self.find_element(alltime).text[3:]
                    self.log.info("第{}个视频总时长为{}".format(index1, video_alltime))
                    # 比较已看时间是否大于总时长
                    assert viedo_watchedtime < video_alltime , self.log.info("第{}个视频已看时长大于总时长".format(index1))
                    index1 += 1
            else :
                self.log.info("该页面没有视频存在")




