# coding: utf-8
from page_locators.login_locators import LoginLocators as ll
from page_objects.home_page import HomePage
from page_objects.message_page import MessagePage
from utils.UI.BasePage import BasePage


class LoginPage(BasePage):
    _switch_index_title = '中教云智慧教学平台'

    def click_login_btn(self):
        """
        官网首页点击登录按钮
        """
        self.find_element(ll.goto_login).click()
        return self

    def enter_user_info(self, username, password):
        """
        输入用户信息
        """
        # 用户名
        self.send_keys(loc=ll.input_user, keyword=username)
        # 密码
        self.send_keys(loc=ll.input_passwd, keyword=password)
        return self

    def click_login(self):
        """
        点击登录数字课程教材云平台按钮
        """
        self.find_element(ll.login_button).click()
        self.find_element(ll.header_button).click()
        self.find_element(ll.to_teacher).click()
        # 切换到新标签页
        self.switch_to_new_handle()

    def click_login_school(self):
        """
        点击登录智慧教学平台
        """
        self.find_element(ll.login_school_button).click()

    def login(self, username, password):
        """
        登录数字课程教材云平台
        """
        self.click_login_btn()
        self.enter_user_info(username=username, password=password)
        self.click_login()

    def login_school(self, username, password):
        """
        登录智慧教学平台
        """
        self.click_login_btn()
        self.enter_user_info(username=username, password=password)
        self.click_login_school()

    def logout(self):
        """
        退出登录
        """
        self.find_element(ll.name).click()
        self.log.info("没有" + str(ll.name) + "元素")
        self.find_element(ll.logout).click()

    def teacher_login(self, teacher_username, teacher_password):
        """
        首次登录教师空间，进入绑定手机号或邮箱页面，则返回True
        """
        self.send_keys(loc=ll.teacher_username, keyword=teacher_username)
        self.send_keys(loc=ll.teacher_password, keyword=teacher_password)
        self.find_element(ll.teacher_login_btn).click()
        if self.isElementExist(ll.tip_title) and self.isElementExist(ll.tip_sub_title):
            return True

