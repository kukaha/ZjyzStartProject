# coding: utf-8
from time import sleep

from utils.UI.BasePage import BasePage
from page_locators.home_locators import HomeLocators as hl
from page_locators.breadcrumb_locators import BreadcrumbLocators as bl
import random


class HomePage(BasePage):

    def get_img(self):
        # loc = '首页'
        return self.isElementExist(hl.img)

    def update_explain(self):
        """
        查看当前首页是否有系统更新说明
        """
        if self.isElementExist(hl.update_explain):
            self.log.info("当前有系统更新说明，点击我知道了关闭弹框")
            self.find_element(hl.know_button).click()
        else:
            self.log.info("当前没有系统更新说明")

    def click_goto_class(self):
        """
        查看首页有几个上课按钮，点击进入上课页面
        """
        count = self.count_elements(hl.goto_class_button)
        if count == 0:
            self.log.info("首页没有去上课按钮")
        else:
            if count == 1:
                self.log.info("首页有一个去上课按钮，点击")
                self.find_element(hl.goto_class_button).click()
            else:
                self.log.info("首页有两个去上课按钮，随机点击一个")
                index = str(random.randint(1, count))
                goto_class_button = hl.get_goto_class_button(index)
                self.find_element(goto_class_button).click()
            self.goto_class()

    def goto_class(self):
        """
        点击去上课，判断是否有PPT，没有直接上课，有的话选择一个PPT去上课
        """

        if self.isElementExist(hl.to_class_button):
            self.log.info("没有课件和班级，直接上课")
        else:
            self.find_element(hl.goto_class_button_again).click()
            if self.isElementExist(hl.count_alert):
                self.log.info("有多个课件，需要先选择课件")
                count = self.count_elements(hl.ppt)
                index = str(random.randint(1, count))
                ppt = hl.select_ppt(index)
                self.find_element(ppt).click()
                self.find_element(hl.goto_class_button_again).click()
        self.find_element(hl.to_class_button).click()
        self.find_element(hl.to_class_button_ok).click()

    def prepare_class(self):
        """
        点击备课按钮后返回首页
        """
        count = self.count_elements(hl.prepare_class_button)
        if count == 0:
            self.log.info("首页没有备课按钮")
        else:
            if count == 1:
                self.log.info("首页有一个备课按钮，点击")
                self.find_element(hl.prepare_class_button).click()
            else:
                self.log.info("首页有两个上课按钮，随机点击一个")
                index = str(random.randint(1, count))
                prepare_class_button = hl.get_prepare_class_button(index)
                self.find_element(prepare_class_button).click()
            self.find_element(bl.home_a).click()

    def goto_programme(self):
        """
        查看当前首页是否有日程，点击进入课程详情页后返回首页
        """
        count = self.count_elements(hl.today_course_icon)
        if count == 0:
            self.log.info("没有今日日程")
        else:
            if count == 1:
                self.log.info("首页有一个课程日程，点击")
                self.find_element(hl.today_course_icon).click()
            else:
                self.log.info("首页有两个课程日程，随机点击一个")
                index = str(random.randint(1, count))
                today_course_icon = hl.today_course_icon(index)
                self.find_element(today_course_icon).click()
            self.find_element(bl.home_a).click()

    def goto_prepare_class(self):
        """
        点击去备课后返回首页
        """
        sleep(1)
        self.find_element(hl.goto_prepare_class).click()
        self.isElementExist(bl.class_list)
        self.log.info("进入到了备授课列表")
        self.find_element(bl.home_a).click()

    def more_book(self):
        """
        点击查看全部教材后返回首页
        """
        self.find_element(hl.all_book).click()
        self.log.info("进入到了数字教材页面")
        self.find_element(bl.home_a).click()

    def correct_homework(self):
        """
        点击批改作业后返回首页
        """
        self.find_element(hl.correct_homework).click()
        self.isElementExist(bl.homework_manage)
        self.log.info("进入到了作业管理页面")
        self.find_element(bl.home_a).click()

    def resource_lib(self):
        """
        点击资源库后返回首页
        """
        self.find_element(hl.resource_lib).click()
        self.isElementExist(bl.resource_list)
        self.log.info("进入到了资源库页面")
        self.find_element(bl.home_a).click()

    def exam_lib(self):
        """
        点击试卷库后返回首页
        """
        self.find_element(hl.exam_lib).click()
        self.isElementExist(bl.exam_list)
        self.log.info("进入到了试卷库页面")
        self.find_element(bl.home_a).click()

    def test_lib(self):
        """
        点击题库后返回首页
        """
        self.find_element(hl.test_lib).click()
        self.isElementExist(bl.test_list)
        self.log.info("进入到了题库页面")
        self.find_element(bl.home_a).click()

    def global_search(self):
        """
        点击搜索按钮后返回首页
        """
        self.find_element(hl.search_icon).click()
        self.log.info("进入到了搜索结果页")
        self.find_element(bl.home_a).click()

    def active(self):
        """
        点击国培活动卡片
        """
        sleep(1)
        self.find_element(hl.active_card).click()
        self.switch_to_new_handle()
        self.log.info("切换到了" + self.browser.title + "窗口")
