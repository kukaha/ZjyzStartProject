# coding: utf-8
from time import sleep

from utils.UI.BasePage import BasePage
from page_locators.book_locators import BookLocators as bookl
import random


class BookPage(BasePage):
    def screen_book(self):
        """
        点击教材页面每个筛选项，查看与查询出来的教材是否一致
        return True
        """
        self.find_element(bookl.e_book).click()
        self.find_element(bookl.subject).click()
        self.find_element(bookl.click_subject(str(2))).click()
        self.find_element(bookl.edition).click()
        self.find_element(bookl.click_edition(str(2))).click()
        self.find_element(bookl.class_button).click()
        self.find_element(bookl.click_class(str(2))).click()
        self.find_element(bookl.volume).click()
        self.find_element(bookl.click_volume(str(2))).click()
        # screen = self.find_elements(el.screen)
        # for list in screen:
        #     sleep(1)
        #     if list.text == "学 科":
        #         list.click()
        #         self.find_element(el.click_subject(str(2))).click()
        #     if list.text == "年 级":
        #         list.click()
        #         self.find_element(el.click_class(str(2))).click()
        #     if list.text == "版 本":
        #         list.click()
        #         self.find_element(el.click_edition(str(2))).click()
        #     if list.text == "册 次":
        #         list.click()
        #         self.find_element(el.click_volume(str(2))).click()
        screen_subject_text = self.find_element(bookl.subject).text.replace(' ', '')
        screen_edition_text = self.find_element(bookl.edition).text.replace(' ', '')
        screen_volume_text = self.find_element(bookl.volume).text.replace(' ', '')
        book_subject_text = self.find_element(bookl.book_subject).text.replace(' ', '')
        book_edition_text = self.find_element(bookl.book_edition).text.replace(' ', '')
        book_volume_text = self.find_element(bookl.book_volume).text.replace(' ', '')
        if screen_subject_text != book_subject_text or screen_edition_text != book_edition_text or screen_volume_text != book_volume_text:
            return False
        return True

    def open_book(self):
        """
        查看用户是否有授权教材，点击打开教材
        """
        self.find_element(bookl.e_book).click()
        count = self.count_elements(bookl.open_book_button)
        if count == 0:
            self.log.info("该用户没有授权教材")
        else:
            if count == 1:
                self.log.info("授权一本数字教材，点击")
                self.find_element(bookl.open_book_button).click()
            else:
                self.log.info("授权两本以上数字教材，随机选择一本点击")
                index = str(random.randint(1, count))
                open_book_button = bookl.select_book(index)
                self.find_element(open_book_button).click()
