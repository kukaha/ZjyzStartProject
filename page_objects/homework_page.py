# coding: utf-8
from time import sleep
from page_locators.breadcrumb_locators import BreadcrumbLocators as bl

from utils.UI.BasePage import BasePage
from page_locators.homework_locators import HomeworkLocators as hwl
import random


class HomeworkPage(BasePage):

    def homework_count(self):
        """
        作业列表页作业总个数
        """
        self.find_element(hwl.homework_manage).click()
        sleep(2)
        count = self.count_elements(hwl.course_count)
        return count

    def homework_name(self, index):
        """
        点击作业名,已经开始的进入作业统计页面
        """
        flag = True
        self.find_element(hwl.name(index, str(1))).click()
        if self.isElementExist(hwl.count_alert):
            self.log.info("作业未开始，暂无统计数据")
        else:
            self.log.info("进入作业统计页面")
            flag = self.isElementExist(bl.homework_statistics)
            self.find_element(bl.homework_manage_a).click()
        return flag

    def course_name(self, index):
        """
        点击相关课程名称进入课程详情页
        """
        course_name_text = self.find_element(hwl.name(index, str(2))).text
        self.find_element(hwl.name(index, str(2))).click()
        course_info_text = self.find_element(hwl.course_info).text
        if course_name_text in course_info_text:
            self.log.info("进入" + course_info_text + "课程详情页")
            self.back_page()

    def exam_answer_type(self, index):
        """
        作业内容没有试卷时，试卷作答方式显示“--”
        """
        flag = True
        operate_text = self.find_element(hwl.operate(index, str(2))).text
        if operate_text != "打印试卷报告":
            type_text = self.find_element(hwl.name(index, str(3))).text
            if type_text != "--":
                self.log.info("当前作业没有试卷，没有作答方式")
                flag = False
        return flag

    def homework_content(self, index):
        """
        查看作业内容:
        资源-查看后关闭弹框
        试卷-查看后点击打印按钮
        """
        self.find_element(hwl.name(index, str(4))).click()
        content_name = self.find_elements(hwl.content(str(1)))
        content_type = self.find_elements(hwl.content(str(2)))
        for i in range(0, content_name.__len__()):
            resource_type_text = content_type.__getitem__(i).text
            if resource_type_text != "试卷":
                content_name.__getitem__(i).click()
                self.find_element(hwl.resource_info_close).click()
                self.log.info('打开' + resource_type_text + '资源后关闭')
            else:
                self.log.info("进入试卷详情页")
                content_name.__getitem__(i).click()
                self.find_element(hwl.set_print_obj).click()
                index = str(random.randint(1, 3))
                self.find_element(hwl.issue_way(index)).click()
                if index == "1":
                    self.find_element(hwl.set_by_class).click()
                else:
                    self.find_element(hwl.set_by_other).click()
                self.log.info("点击打印按钮")
                self.find_element(hwl.set_ok).click()
                sleep(1)
                self.find_element(hwl.cancel).click()
                sleep(2)
                self.find_element(hwl.content(str(3))).click()
                if self.isElementExist(hwl.cancel):
                    self.find_element(hwl.cancel).click()
                else:
                    self.log.info("获取试卷信息失败")
                    return False
        self.find_element(hwl.close).click()
        return True

    def operate(self, index):
        """
        作业操作项
        未开始的作业：编辑、作业统计、打印试卷报告、删除
        已开始的作业：作业统计、打印试卷报告、删除
        """
        operate_text = self.find_element(hwl.operate(index, str(1))).text
        self.find_element(hwl.operate(index, str(1))).click()
        flag = True
        if operate_text == "编辑":
            self.log.info("作业未开始，点击编辑按钮编辑作业")
            sleep(2)
            self.find_element(hwl.issue).click()
            if not self.isElementExist(hwl.issue_alert):
                flag = False
        elif operate_text == "作业统计":
            self.log.info("进入作业统计页面")
            if not self.isElementExist(bl.homework_statistics):
                flag = False
            self.find_element(bl.homework_manage_a).click()
        operate_text = self.find_element(hwl.operate(index, str(2))).text
        self.find_element(hwl.operate(index, str(2))).click()
        if operate_text == "删除":
            self.log.info("删除作业2")
            if not self.isElementExist(hwl.delete_started_explain):
                flag = False
            self.find_element(hwl.cancel).click()
            if not self.isElementExist(hwl.delete_alert):
                flag = False
        else:
            if operate_text == "作业统计":
                self.log.info("点了作业统计按钮，是未开始的作业")
                if not self.isElementExist(hwl.count_alert):
                    flag = False
            else:
                if self.isElementExist(bl.print_report):
                    self.find_element(bl.homework_manage_a).click()
                else:
                    self.log.info("该试卷已批改数为0，不能点击打印试卷报告")
            if self.isElementExist(hwl.operate(index, str(3))):
                operate_text = self.find_element(hwl.operate(index, str(3))).text
                self.find_element(hwl.operate(index, str(3))).click()
                if operate_text == "删除":
                    self.log.info("删除作业3")
                    if self.isElementExist(hwl.delete_started_explain):
                        self.log.info("作业已经开始")
                    else:
                        self.find_element(hwl.delete_not_started_explain)
                        self.log.info("作业未开始")
                    self.find_element(hwl.cancel).click()
                    if not self.isElementExist(hwl.delete_alert):
                        flag = False
                else:
                    self.log.info("该试卷未开始，已批改数为0，不能点击打印试卷报告")
                    if self.isElementExist(hwl.operate(index, str(4))):
                        self.find_element(hwl.operate(index, str(4))).click()
                        self.log.info("删除作业4")
                        if not self.isElementExist(hwl.delete_started_explain):
                            flag = False
                        self.find_element(hwl.cancel).click()
                        if not self.isElementExist(hwl.delete_alert):
                            flag = False
        return flag
