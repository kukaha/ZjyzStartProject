# coding=utf-8
# @time   :2021/08/25
# @Author :wushaotang
# @File   : analyse_locators.py
# @Description: 中教云智慧教学平台-学情分析
# @Return  :
import re

from selenium.webdriver.common.by import By
from utils.UI.BasePage import BasePage


class AnalyseLocators(BasePage):


    #菜单：“学情分析”
    menu_analyse = (By.XPATH, '//aside/div[1]/div[1]/ul//span[contains(text(),"学情分析")]')
    #平均每日在线时长
    ave_time = (By.XPATH, '//div[@class="learn_subject"]/div[1]/div[1]/div/p')
    #章节掌握情况
    zhangjie = (By.XPATH, "//div[@class='learn_subject']/div[4]/h3")

    #菜单TAB页：学生学情
    student_analyse  =  (By.XPATH, '//div[@class="ant-tabs-nav-wrap"]/div/div[2]/div')
    #知识点分析
    answer = (By.XPATH, '//div[@class="student-content"]/div[2]/div/div/div/a[1]')
    #学情分析
    student_analysis = (By.XPATH, '//div[@class="student-content"]/div[2]/div/div/div/a[2]')
