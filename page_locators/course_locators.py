# coding=utf-8
# @time   :2021/07/27
# @Author :wushaotang
# @File   : course_locators.py
# @Description: 中教云智慧教学平台-备授课
# @Return  :
import re

from selenium.webdriver.common.by import By
from utils.UI.BasePage import BasePage


class CourseLocators(BasePage):


    #菜单‘备授课’
    menu_course = (By.XPATH, '//*[@id="rootTeacher"]/section/aside/div[1]/div[1]/ul/li[2]')
    # 左侧第一个章节（除‘全部’外）
    left_courser = (By.XPATH, '//*[@id="teaching-my-urriculum-first"]/div/div/div[2]/div[3]/div/div/div/div[2]/span[3]/span[2]/div/span')
    #我的课程数列表
    d_courses = (By.XPATH, '//*[@id="rootTeacher"]/section/section/main/div/div/div/div[2]/div[2]/div/div/div[1]/div/ul/div')
    #右下角的课程总记数：   共 12 条
    t_count_coursers = (By.XPATH, '//*[@id="rootTeacher"]/section/section/main/div/div/div/div[2]/div[2]/div/div/div[2]/ul/li[1]')
    #右上角创建课程按钮
    d_new_courses = (By.ID, 'teaching-my-urriculum-third')
    #弹框：
    #弹框 章节下拉框：
    i_new_zhangjie =  (By.XPATH, '//*[@id="rootTeacher"]/div/div/div[2]/div/div[2]/div[2]/div/form/div[2]/div[2]/div/span/div/div/span[2]')
    #弹框 章节下拉框第一个位置
    i_zhangjie_1 = (By.XPATH, '/html/body/div[6]/div/div/div/div/div/div[3]/div[1]/div/div/div[1]/span[3]/span')
    #弹框 新建课程的名字输入框
    i_courser_name = (By.ID, 'add-course_courseName')
    #弹框  翻转课堂
    lab_rotate = (By.XPATH, '//*[@id="add-course_teachingModel"]/label[1]')
    #弹框  自定义课堂
    lab_user_defined = (By.XPATH, '//*[@id="add-course_teachingModel"]/label[2]/span[2]')
    #弹框  提交
    b_submit = (By.XPATH, '//*[@id="rootTeacher"]/div/div/div[2]/div/div[2]/div[3]/button[2]')

