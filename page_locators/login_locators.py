# coding: utf-8
from selenium.webdriver.common.by import By


class LoginLocators:

    # 进入登录页按钮
    goto_login = (By.XPATH, '//*[@id="website-header"]/div[2]/div[2]/div/div/a[2]')
    # 用户名
    input_user = (By.XPATH, '//*[@id="username"]')
    # 密码
    input_passwd = (By.XPATH, '//*[@id="password"]')
    # 登录数字课程教材云平台
    login_button = (By.XPATH, '//span[text()="登录数字课程教材云平台"]')
    header_button = (By.XPATH, '//*[@id="website-header"]/div[2]/div[2]/div/div/h4')
    to_teacher = (By.XPATH, '//span[text()="中教云智慧教学平台"]')
    # 登录智慧教学平台
    login_school_button = (By.XPATH, '//span[text()="登录智慧教学平台"]')
    # 退出登录
    name = (By.XPATH, '//*[@class="content-span"]')
    logout = (By.XPATH, '//a[text()="退出"]')
    # 教师空间用户名
    teacher_username = (By.XPATH, '//input[@id="username"]')
    # 教师空间密码
    teacher_password = (By.XPATH, '//input[@id="password"]')
    teacher_login_btn = (By.XPATH, '//button[contains(@class,"ant-btn")]')
    # 首次登录教师空间提示
    tip_title = (By.XPATH, '//div[@class="validate-tips"]/h6')
    tip_sub_title =(By.XPATH, '//div[@class="validate-tips"]/p')