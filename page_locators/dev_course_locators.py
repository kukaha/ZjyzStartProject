# coding=utf-8
# @time   :2021/08/27
# @Author :wushaotang
# @File   : dev_course_locators.py
# @Description: 中教云智慧教学平台-开发-备授课
# @Return  :
import re

from selenium.webdriver.common.by import By
from utils.UI.BasePage import BasePage


class DevCourseLocators(BasePage):


    #备授课菜单
    menu_course = (By.XPATH, '//*[@id="rootTeacher"]/section/aside/div[1]/div[1]/ul/li[2]')
    #推荐课程第1位
    recommend_course = (By.XPATH, '//tbody[@class="ant-table-tbody"]/tr[1]/td')
    #弹出课程的title
    course_title_n =  (By.XPATH, '//div[@class="ant-modal-header"]/div')
    #弹出框课前所包含的资源（第一位）
    recommend_resource =  (By.XPATH, '//div[@class="ant-tabs-tabpane ant-tabs-tabpane-active recommend-tab"]/div[1]')
    #弹出课程-资源详情的关闭按钮
    recommend_resource_close =  (By.XPATH, '//*[@id="rootTeacher"]/div[3]/div/div[2]/div/div[2]/button/span')
    #弹出课程-关闭按钮
    recommend_close =  (By.XPATH,'//*[@id="rootTeacher"]/div[2]/div/div[2]/div/div[2]/button/span')

    #课程列表第一节课
    course_list_1 = (By.XPATH, '//ul[@class="ant-list-items"]/div[1]/div[1]/div[1]/b')
    #‘共享’按钮
    share_butten = (By.XPATH, '//header/div[2]/span[2]')
    #'校内共享'
    school_share = (By.XPATH, '//div[@class="ant-radio-group ant-radio-group-outline"]/label[2]')
    #提交共享
    share_submit = (By.XPATH, '//div[@class="ant-modal-footer"]/button[2]')
    #'校内/云共享‘的位置
    share_location = (By.XPATH, '//div[@class="to-class-right"]/div[2]/div/div[3]/div[1]')
    #鼠标指向共享记录
    mouse_location  =  (By.XPATH, '//div[@class="to-class-right"]/div[2]/div[1]')
    #’撤销‘的位置
    revortion =  (By.XPATH, '//*[@id="rootTeacher"]/section/section/main/div[2]/div/div[2]/div[1]/div[1]/div[3]/div[1]')
    #'确认“
    submit =  (By.XPATH,  '//div[@class="ant-modal-content"]/div/div/div[3]/button[2]')
    #'共享记录'按钮
    share_log = (By.XPATH, '//header/div[2]/span[3]')
    #弹框
    pop_up = (By.XPATH, '/html/body/div[6]/div')

    #教案
    teaching_plan = (By.XPATH, '//header/div[2]/span[1]')
    #课件
    teaching_1 =  (By.XPATH,'//div[@class="plan-edit-header"]/div[2]/div/div/div/div[1]/div[1]')
    #课件
    teaching_2 =  (By.XPATH,'//div[@class="plan-edit-header"]/div[2]/div/div/div/div[1]/div[2]')
    #课件
    teaching_3 =  (By.XPATH,'//div[@class="plan-edit-header"]/div[2]/div/div/div/div[1]/div[3]')
    #右侧列表第一个位置
    right_one = (By.XPATH,'//div[@class="right"]/div/div/div/div/div[1]')