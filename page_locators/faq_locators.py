from selenium.webdriver.common.by import By

from utils.UI.BasePage import BasePage


class FAQLocators(BasePage):

    # 菜单‘帮助中心’
    menu_faq = (By.XPATH,'//*[@class="teach-side"]/ul/li[12]')
    # 左侧菜单列表
    faq_menulist = (By.XPATH,'//*[@class="help-l"]/ul/li')
    # 右侧视频列表
    videos_count = (By.XPATH,'//*[@class="help-video-list"]/div[2]/div')
    # 中间视频
    video = (By.XPATH,'//*[@id="CXO_Editor_SDK"]')


    @staticmethod
    def find_menu_byid(index):
        menu_byid = (By.XPATH,'//*[@class="help-l"]/ul/li'+'['+index+']')
        return menu_byid

    @staticmethod
    def get_video_watchedtime(index1):
        video_watchedtime = (By.XPATH,'//*[@class="help-video-list"]/div[2]/div['+index1+']/div[2]/div[1]')
        return video_watchedtime

    @staticmethod
    def get_video_alltime(index2):
        video_alltime = (By.XPATH,'//*[@class="help-video-list"]/div[2]/div['+index2+']/div[2]/div[2]')
        return video_alltime