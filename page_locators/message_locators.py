from selenium.webdriver.common.by import By


class MessageLocators:
    # 消息按钮
    message_button = (By.XPATH, '//*[@class="ant-badge header-badge"]/img')
    # 消息标题
    message_title = (By.XPATH, '//*[@class="ant-layout-content teach-content"]/div/div/div/div[1]/div[1]')
    # 消息中链接不存在弹框
    alert = (By.XPATH, '//*[@class="ant-message"]//span[2]')

    # 获取消息链接
    @staticmethod
    def get_message_link(index):
        message_link = (By.XPATH, '//*[contains(@class,"teach-content")]/div/div/div[' + index + ']/div[2]/span')
        return message_link

    # 获取消息标题
    @staticmethod
    def get_message_title(index):
        message_title = (By.XPATH, '//*[contains(@class,"teach-content")]/div/div/div[' + index + ']/div[1]/div[1]')
        return message_title
