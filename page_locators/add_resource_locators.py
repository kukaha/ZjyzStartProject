# coding=utf-8
# @time   :2021/07/27
# @Author :wushaotang
# @File   : course_locators.py
# @Description: 中教云智慧教学平台-备授课-添加资源
# @Return  :



import re

from selenium.webdriver.common.by import By
from utils.UI.BasePage import BasePage


class AddResourceLocators:


    # 课程类型位置：（自定义/翻转）
    # 第一节课的类型位置
    courser_type_1 = '//*[@id="teaching-my-urriculum-second"]/div[1]/div[1]/span'
    # 第二节课程的类型位置
    courser_type_2 = '//*[@id="rootTeacher"]/section/section/main/div/div/div/div[2]/div[2]/div/div/div[1]/div/ul/div[2]/div[1]/div[1]/span'

    # 课程详情
    # 翻转课堂 右上角 添加资源
    add_resource = (By.ID, 'Select')
    # add_resource = (By.XPATH, '//*[@id="Select"]')

    #课程类型位置列表
    @staticmethod
    def course_type_list():
        course_type_list = [(By.XPATH, '//*[@id="teaching-my-urriculum-second"]/div[1]/div[1]/span')]
        for i in range(2, 7):
            courser_type_n = (By.XPATH,
                              '//*[@id="rootTeacher"]/section/section/main/div[1]/div/div/div[2]/div[2]/div/div/div[1]/div/ul/div[{}]/div[1]/div[1]/span'.format(
                                  i))
            course_type_list.append(courser_type_n)
        return course_type_list

    #课程名称位置列表
    @staticmethod
    def course_location():
        course_type_list = [(By.XPATH, '//*[@id="teaching-my-urriculum-second"]/div[1]/div[1]/b')]
        for i in range(2, 7):
            courser_type_n = (By.XPATH,
                              '//*[@id="rootTeacher"]/section/section/main/div[1]/div/div/div[2]/div[2]/div/div/div[1]/div/ul/div[{}]/div[1]/div[1]/b'.format(
                                  i))
            course_type_list.append(courser_type_n)
        return course_type_list

    # 添加资源的弹框 资源库
    resource_warehouse = (By.XPATH, '//*[@id="rootTeacher"]/div[2]/div/div/div/div[2]/div[2]/div/div[1]/p[1]')

    # 资源库弹窗  资源是否添加的状态（右上角）
    def if_add_resource_location(n):
        nn = str(n)
        return (By.XPATH,'//*[@id="ResourceList_wrap"]/div[1]/div[7]/div/div/div[1]/div[{}]/div/ul/li[1]/img'.format(nn))

    # 资源库弹窗  资源名
    def add_resource_name(n):
        return (By.XPATH,'//*[@id="ResourceList_wrap"]/div[1]/div[7]/div/div/div[1]/div[{}]/div/ul/li[2]'.format(n))

    #资源弹框-资源添加提交 按钮
    resource_submit = (By.XPATH,'//*[@id="ResourceList_wrap"]/div[3]/button')

    #资源列表页-资源名位置
    def resource_name_list(index):
        return (By.XPATH, '//*[@id="rootTeacher"]/section/section/main/div[2]/main/div[1]/div[1]/div/main[1]/div[2]/div[{}]/section[1]/span[1]'.format(index))

    #资源--自定义
    #自定义课程详情页--添加资源+
    custom_add = (By.XPATH, '//*[@id="rootTeacher"]/section/section/main/div[2]/main/div[1]/div/div/main/div[1]/span')

    #添加作业-翻转
    add_job = (By.XPATH, '//*[@id="rootTeacher"]/section/section/main/div[2]/main/div[1]/div[1]/div/main[2]/div[1]/div[2]/span/img')
    #布置作业弹框-课前资源第一行
    resource_1 = (By.XPATH, '//*[@id="rootTeacher"]/div[2]/div/div[2]/div/div[2]/div[1]/div/div/main/div[1]/div[3]/li[1]/span')
    #布置作业弹框-添加全部
    add_all = (By.XPATH, '//*[@id="rootTeacher"]/div[3]/div/div[2]/div/div[2]/div[1]/div/div/main/div[1]/div[1]/span[2]')
                            # '//*[@id="rootTeacher"]/div[3]/div/div[2]/div/div[2]/div[1]/div/div/main/div[1]/div[1]/span[2]'
    #布置作业弹框-下一步
    next_button = (By.XPATH, '//button/span[contains(text(),"下一步")]')

    #布置作业弹框-发布设置-作业名称
    job_name = (By.ID, 'homeworkName')
    #设置布置作业的作答时间-统一时间
    job_time = (By.XPATH, '//span[contains(text(),"统一时间")]')
    #设置-今天
    job_today = (By.XPATH, '//span[contains(text(),"今天")]')
    #设置-试卷作答方式-在线作答
    job_mode = (By.XPATH, '//span[contains(text(),"在线作答")]')
    #设置-作业发布
    job_submit = (By.XPATH, '//button/span[contains(text(),"发 布")]')

    #添加作业--自定义
    add_job_custom = (By.XPATH, '//button/span[contains(text(),"布置作业")]')
    #自定义-课前资源第一行
    resource_custom_1 = (By.XPATH, '//main/div/div/li[1]')
    #自定义-课程详情-作业页
    job_page = (By.XPATH, "//nav/div/span[contains(text(), '作业')]")
