# coding: utf-8
from selenium.webdriver.common.by import By


class CloudResourceLocators:
    # 资源卡片
    resource_card = (By.XPATH, '//div[@class="resource-main-box"]//li[contains(@class,"ant-list-item")]/div/div/div')
    # 课程名
    course_name = (By.XPATH, '//div[@class="header-left-title"]')
    # 资源名
    resource_name = (By.XPATH, '//div[@class="resource-preview-top-title"]/div')
    # 资源收藏成功提示
    collect_alert = (By.XPATH, '//div[@class="ant-message-notice"]//span[2]')
    # 已添加资源提示
    added_alert = (By.XPATH, '//span[text()="当前环节中已存在该资源！"]')
    # 资源详情返回按钮
    resource_back = (By.XPATH, '//div[@class="resource-preview-header_buttons"]/div[1]/button')

    # 资源类型
    @staticmethod
    def resource_type(index):
        resource_type = (By.XPATH, '//div[@class="resource-list-nav"]/div[1]/div/div/a[' + index + ']')
        return resource_type

    # 资源卡片可操作项（div_index：1、查看2、添加到课程3、收藏）
    @staticmethod
    def resource_card_operate(li_index, div_index):
        resource_card_operate = (By.XPATH, '//div[@class="resource-main-box"]//li[contains(@class,"ant-list-item")][' + li_index + ']/div/div/div/div[' + div_index +']/img')
        return resource_card_operate

    # 资源卡片可操作项文本（div_index：1、查看2、添加到课程3、收藏）
    @staticmethod
    def resource_operate_text(li_index, div_index):
        resource_operate_text = (By.XPATH, '//div[@class="resource-main-box"]//li[contains(@class,"ant-list-item")][' + li_index + ']/div/div/div/div[' + div_index + ']/span')
        return resource_operate_text
