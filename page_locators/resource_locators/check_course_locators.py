# coding: utf-8
from selenium.webdriver.common.by import By


class CheckCourseLocators:
    # 教案
    teaching_plan = (By.XPATH, '//div[@class="course-main"]/div[2]/div/div[2]/span[1]')
    # alert提示
    alert = (By.XPATH, '//div[@class="ant-message-notice"]//span[2]')
    # 添加教案到课程/未授权
    add_teaching_plan_button = (By.XPATH, '//div[@class="course-main"]/div[2]/div/div[2]/div')
    add_resource_list = (By.XPATH, '//div[@class="course-main"]/div[3]/div/div/div/span[1]')
    # 共享记录按钮
    share_history = (By.XPATH, '//button[@class="ant-btn header-right-version"]')
    share_history_version = (By.XPATH, '//div[@class="ant-popover-inner"]/div[2]/div[1]/div')
    version_text = (By.XPATH, '//div[@class="ant-popover-inner"]/div[2]/div/div/div/div')
    course_version_text = (By.XPATH, '//div[@class="main-header"]/div[1]/div[1]')
    # 关闭资源
    close = (By.XPATH, '//span[@class="ant-modal-close-x"]/span')
    # 课程名
    course_name = (By.XPATH, '//div[@class="header-left-title"]')
    # 引用此课程按钮
    use_course_button = (By.XPATH, '//span[text()="引用此课程"]')
    use_course_warn = (By.XPATH, '//div[@class="noAuthorizedModal"]/div[1]')
    no_author_resource = (By.XPATH, '//div[@class="noAuthorizedModal"]/div[2]')

    # 添加资源到课程/未授权
    @staticmethod
    def add_resource_button(index):
        add_resource_button = (By.XPATH, '//div[@class="course-main"]/div[3]/div/div/div[@class="common-cource-resource-item"][' + index + ']/span[4]')
        return add_resource_button
    # 课堂模式
    step_header_list = (By.XPATH, '//div[@class="course-main"]/div[3]//div[@class="ant-tabs-nav-list"]/div/div')

    # 课堂模式（1、备资源/课前2、课中/备课件3、课后）
    @staticmethod
    def step_header(index):
        step_header = (By.XPATH, '//div[@class="course-main"]/div[3]//div[@class="ant-tabs-nav-list"]/div[' + index + ']/div')
        return step_header
    # 课程环节中分类
    sort = (By.XPATH, '//div[@class="course-main"]/div[3]//div[@class="sort-header"]')
    # 未授权鼠标悬停
    accredit_hover = (By.XPATH, '//div[text()="您所在的学校或区域未购买该资源"]')
    # 未授权提示
    accredit_alert = (By.XPATH, '//span[text()="未获取权限"]')
    # 教案详情名称
    resource_detail_name = (By.XPATH, '//div[@class="ant-modal-content"]/div[1]/div')

    # 添加到课程选项
    belong_property = (By.XPATH, '//form[contains(@class,"ant-legacy-form")]/div/div[2]/div/span/div/div[1]//span[2]')

    # 添加资源弹框（1、选择教材2、选择章节3、选择课程4、选择环节）
    @staticmethod
    def add_resource_to_class(index):
        add_resource_to_class = (By.XPATH, '//form[contains(@class,"ant-legacy-form")]/div[' + index + ']/div[2]/div/span/div/div[1]')
        return add_resource_to_class
    # 创建课程
    creat_course = (By.XPATH, '//form[contains(@class,"ant-legacy-form")]/div[3]/div[1]//a')
    # 第一本教材，章节
    first_book = (By.XPATH, '//*[@id="used-recommend-course_eBookId_list"]/following-sibling::div/div/div/div/div[1]')
    first_catalog = (By.XPATH, '//form[contains(@class,"ant-legacy-form")]//div[@class="ant-select-tree-list-holder-inner"]/div[1]')
    # 课程列表
    course_list = (By.XPATH, '//div[@class="rc-virtual-list"]/div[1]/div/div/div')
    # 确认
    confirm = (By.XPATH, '//span[text()="确 认"]')
    confirm1 = (By.XPATH, '//span[text()="确 定"]')
    cancel = (By.XPATH, '//span[text()="取 消"]')

    # 选择课程
    @staticmethod
    def select_course_name(index):
        course_name = (By.XPATH, '//div[@class="rc-virtual-list"]/div[1]/div/div/div[' + index + ']/div')
        return course_name

    # 环节列表
    step_list = (By.XPATH, '//div[@id="used-recommend-course_stepId_list"]/following-sibling::div/div[1]/div/div/div')

    # 选择课程环节（1、课前2、课中3、课后）
    @staticmethod
    def course_step(index):
        course_step = (By.XPATH, '//div[@id="used-recommend-course_stepId_list"]/following-sibling::div/div[1]/div/div/div[' + index + ']')
        return course_step

    # 资源详情操作按钮（1、收藏2、添加到课程）
    @staticmethod
    def resource_detail(index):
        resource_detail = (By.XPATH, '//div[@class="ant-modal-content"]/div[3]/button[' + index + ']/span')
        return resource_detail

    course_resource_list = (By.XPATH, '//div[@class="course-main"]/div[3]/div/div/div/span[1]')

    # 课程环节中资源
    @staticmethod
    def course_resource(index):
        course_resource = (By.XPATH, '//div[@class="course-main"]/div[3]/div/div/div[@class="common-cource-resource-item"][' + index + ']/span[1]')
        return course_resource

    recommend_course_list = (By.XPATH, '//div[@class="resource-recommend-panel"]/div/div[2]')

    # 推荐课程
    @staticmethod
    def recommend_course(index):
        recommend_course = (By.XPATH, '//div[@class="resource-recommend-panel"]/div[' + index + ']/div/div[1]')
        return recommend_course

    # 课程相关数量（1、总使用量2、总收藏量）
    @staticmethod
    def courses_count(index):
        count = (By.XPATH, '//div[@class="resource-info-panel"]/div[1]/div[' + index + ']/span')
        return count

    # 课程所属信息（1、共享日期2、所属教材3、所属章节）
    @staticmethod
    def course_info(index):
        info = (By.XPATH, '//div[@class="resource-info-panel"]/div[2]/div[1]/div[' + index + ']')
        return info

    # 课程创建人信息（1、创建人2、创建人学校）
    @staticmethod
    def course_creator(index):
        creator = (By.XPATH, '//div[@class="resource-info-panel"]/div[2]/div[2]/div[2]/div[' + index + ']')
        return creator
