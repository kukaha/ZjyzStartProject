# coding: utf-8
from selenium.webdriver.common.by import By


class CheckResourceLocators:
    # 跳转到课程链接
    skip_link_to_course = (By.XPATH, '//div[@class="ant-modal-body"]//span')
    # 新建课程链接
    create_course = (By.XPATH, '//form[contains(@class,"ant-legacy-form")]/div[3]/div[1]//a')
    # 二维码图片
    qr_code_img = (By.XPATH, '//div[@class="code-modal-img"]/img[1]')
    # 关闭二维码弹框
    close = (By.XPATH, '//div[@class="ant-modal-content"]/button/span/span')
    # 错误类型
    error_type = (By.XPATH, '//div[@class="ant-checkbox-group"]/label[1]/span[1]')
    error_description = (By.XPATH, '//textarea[@id="errorDesc"]')
    confirm1 = (By.XPATH, '//span[text()="确 认"]')

    # 资源详情按钮（1、添加到课程2、收藏3、资源二维码4、纠错）
    @staticmethod
    def buttons(index):
        button = (By.XPATH, '//div[@class="resource-preview-header_buttons"]/div[3]/button[' + index + ']')
        return button

    collect_button_text = (By.XPATH, '//div[@class="resource-preview-header_buttons"]/div[3]/button[2]//span[2]')
    add_to_course_option_list = (By.XPATH, '//div[@class="ant-modal-body"]/form/div/div/div/div[2]')

    # 输入课程名称提示
    course_name_warn = (By.XPATH, '//div[text()="请输入课程名称"]')
    step_warn = (By.XPATH, '//div[text()="请选择教学环节"]')
    confirm = (By.XPATH, '//span[text()="确 定"]')
    cancel = (By.XPATH, '//span[text()="取 消"]')

    # 添加到课程选项(课程和环节)
    add_to_select_course = (By.XPATH, '//div[@class="ant-modal-body"]/form/div[3]/div/div/div[2]/div/span/div/div/div')
    add_to_select_step = (By.XPATH, '//div[@class="ant-modal-body"]/form/div[4]/div/div/div[2]/div/span/div/div/span[1]')

    step_count = (By.XPATH, '//div[@class="ant-modal-body"]/form/div[4]/div//div[@class="rc-virtual-list-holder"]/div/div/div')

    # 课程环节
    @staticmethod
    def step(index):
        step_list = (By.XPATH, '//div[@class="ant-modal-body"]/form/div[4]/div//div[@class="rc-virtual-list-holder"]/div/div/div[' + index + ']')
        return step_list

    # 推荐资源列表
    recommend_resource_list = (By.XPATH, '//div[@class="recommend-list-container"]/div')

    # 推荐资源
    @staticmethod
    def recommend_resource(index):
        recommend_resource = (By.XPATH, '//div[@class="recommend-list-container"]/div[' + index + ']/div[2]/div[1]')
        return recommend_resource

    # 资源弹框资源名称
    resource_name = (By.XPATH, '//div[@class="ant-modal-content"]/div[1]/div')


