# coding: utf-8
from selenium.webdriver.common.by import By


class UploadResourceLocators:
    # 资源库导航
    resource = (By.XPATH, '//*[contains(@class,"teach-side-box ")][5]')
    # 上传资源按钮
    upload_resource = (By.XPATH, '//span[text()="上传资源"]')
    # 选择文件
    select_file = (By.ID, 'fileObj')
    # 文件上传成功alert
    select_file_alert = (By.XPATH, '//div[@class="ant-message-notice-content"]')
    # 附件
    annex_name = (By.XPATH, '//span[@class="ant-upload-list-item-name"]')
    # 资源名称输入框
    input_resource_name = (By.XPATH, '//div[@class="horizontal_upload"]/form/div[2]//input')
    # 资源类型输入框
    input_resource_type = (By.XPATH, '//form/div[3]//div[@class="ant-select-selector"]/span[1]')
    # 所属教材输入框
    input_book = (By.XPATH, '//div[@class="horizontal_upload"]/form/div[5]/div/div/div/div/div')
    # 所属章节输入框
    input_catalog = (By.XPATH, '//form/div[6]//div[@class="ant-select-selection-overflow"]')
    # 所属知识点输入框
    input_knowledge_point = (By.XPATH, '//form/div[7]//div[@class="ant-select-selection-overflow-item"]')
    # 确认按钮
    confirm = (By.XPATH, '//div[@class="ant-modal-footer"]//button[2]')
    # 取消按钮
    cancel = (By.XPATH, '//div[@class="ant-modal-footer"]//button[1]')
    # 上传资源重名提示
    same_name_alert = (By.XPATH, '//div[@class="ant-message-notice-content"]//span[2]')
    # 推荐资源名
    recommend_name = (By.XPATH, '//p[text()="推荐名字："]/span')

    # 上传资源弹框中的警告提示（1、选择文件2、设置资源名称3、选择资源类型5、选择教材6、选择章节）
    @staticmethod
    def warn(index):
        warn = (By.XPATH, '//form/div[' + index + ']//div[contains(@class,"ant-form-item-explain")]')
        return warn

    # 设置共享模式(1、云共享2、校内共享3、不共享)
    @staticmethod
    def share_model(index):
        share_model = (By.XPATH, '//div[@id="shareModel"]/label[' + index + ']')
        return share_model

    resource_type_count = (By.XPATH, '//form/div[3]//div[@class="rc-virtual-list-holder"]/div/div[1]/div')

    # 上传资源，资源类型下拉框
    @staticmethod
    def resource_type(index):
        resource_type_list = (By.XPATH, '//form/div[3]//div[@class="rc-virtual-list-holder"]/div/div[1]/div[' + index + ']/div[1]')
        return resource_type_list

    book_count = (By.XPATH, '//div[@class="horizontal_upload"]/form/div[5]//div[@class="rc-virtual-list"]/div[1]/div/div/div')

    # 上传资源教材列表
    @staticmethod
    def belong_book(index):
        book_list = (By.XPATH, '//div[@class="horizontal_upload"]/form/div[5]//div[@class="rc-virtual-list"]/div[1]/div/div/div[' + index +']')
        return book_list

    catalog_count = (By.XPATH, '//form/div[6]//div[@class="ant-select-tree-list-holder"]/div/div/div/span[3]')

    # 上传资源章节列表
    @staticmethod
    def belong_catalog(index):
        catalog_list = (By.XPATH, '//form/div[6]//div[@class="ant-select-tree-list-holder"]/div/div/div[' + index + ']/span[3]')
        return catalog_list

    knowledge_count = (By.XPATH, '//form/div[7]//div[@class="ant-select-tree-list-holder"]/div/div/div/span[3]')

    # 上传资源知识点列表
    @staticmethod
    def belong_knowledge(index):
        knowledge_list = (By.XPATH, '//form/div[7]//div[@class="ant-select-tree-list-holder"]/div/div/div[' + index + ']/span[3]')
        return knowledge_list
