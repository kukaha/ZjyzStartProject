# coding: utf-8
from selenium.webdriver.common.by import By


class CheckCombineResourceLocators:
    # 专题tab
    topic_tab = (By.XPATH, '//div[contains(@class,"ant-tabs-card")]/div[1]/div[1]/div/div[2]')
    # 组合资源
    resource_type = (By.XPATH, '//div[@class="resource-list"]/div[1]/div[3]/div//a[2]')

    # 组合资源详情内资源列表
    resource_list = (By.XPATH, '//div[@class="preview-list-part"]/div/div[1]/div[1]')

    @staticmethod
    def resource_name(index):
        """
        资源名称
        """
        resource_name = (By.XPATH, '//div[@class="preview-list-part"]/div[' + index + ']/div[1]/div[1]')
        return resource_name

    @staticmethod
    def resource_collect(index):
        """
        收藏资源按钮
        """
        resource_collect = (By.XPATH, '//div[@class="preview-list-part"]/div[' + index + ']/div[2]/div[1]')
        return resource_collect

    @staticmethod
    def resource_add(index):
        """
        添加资源按钮
        """
        resource_add = (By.XPATH, '//div[@class="preview-list-part"]/div[' + index + ']/div[2]/div[2]')
        return resource_add

    # 资源简介（collect_index：厂商和收录时间，title_index：使用量和收藏量）
    @staticmethod
    def resource_info_header(collect_index, title_index):
        resource_info_header = (By.XPATH, '//div[@class="info-header"]/div[' + collect_index + ']/div[' + title_index + ']')
        return resource_info_header

    resource_info_title = (By.XPATH, '//div[@class="info-text"]/div/div[1]')
    # 资源所属信息
    resource_info_content = (By.XPATH, '//div[@class="info-text"]/div/div[2]')
    # 视频iframe
    iframe_loc = (By.XPATH, '//div[@class="conbi-detail-preview-content"]/div/iframe')
    # 95教研室视频播放按钮
    play_btn = (By.XPATH, '//div[@class="pv-controls"]/div[1]/button')
    player = (By.XPATH, '//div[@id="player"]/div')
    # 博雅云课堂视频播放按钮
    play_btn2 = (By.XPATH, '//div[@class="vjs-control-bar"]/button[1]')
    player2 = (By.XPATH, '//div[@id="example_video_1"]')
    content_text = (By.XPATH, '//div[@id="text-activity-content"]/p/span')
    content_img = (By.XPATH, '//div[@id="text-activity-content"]/p/img')
