from selenium.webdriver.common.by import By


class HomeLocators:
    # 左上角头像
    img = (By.XPATH, '//*[@class="teach-logo"]/img')
    # 首页面包屑
    index_breadcrumb = (By.XPATH, '//a[text()="首页"]')
    # 查看全部教材
    all_book = (By.XPATH, '//p[text()="查看全部教材"]')
    # 去备课
    goto_prepare_class = (By.XPATH, '//span[text()="去备课"]')
    # 批改作业
    correct_homework = (By.XPATH, '//span[text()="批改作业"]')
    # 资源库
    resource_lib = (By.XPATH, '//div[text()="资源库"]')
    # 试卷库
    exam_lib = (By.XPATH, '//div[text()="试卷库"]')
    # 题库
    test_lib = (By.XPATH, '//div[text()="题库"]')
    # 国培活动
    active_card = (By.XPATH, '//div[contains(@class,"slick-active slick-current")]/div/div/img')
    # 下课按钮
    to_class_button = (By.XPATH, '//p[text()="下课"]')
    # 确定下课
    to_class_button_ok = (By.XPATH, '//span[text()="确 定"]')
    # 未选择课件的弹框
    count_alert = (By.XPATH, '//span[text()="请选择课件"]')
    # 确定去上课
    goto_class_button_again = (By.XPATH, '//span[text()="去上课"]')
    # 搜索按钮
    search_icon = (By.XPATH, '//*[@class="ant-input-affix-wrapper ant-input-affix-wrapper-borderless"]//span/span')
    # 系统更新说明弹框
    update_explain = (By.XPATH, '//*[@id="rootTeacher"]/div[2]/div/div[2]/div/div[2]/div')
    # 我知道了按钮
    know_button = (By.XPATH, '//button[contains(@class,"message-ok")]')

    # 上课按钮
    goto_class_button = (By.XPATH, '//*[contains(@class,"ant-list-item")]/div[3]/button[1]')

    @staticmethod
    def get_goto_class_button(index):
        goto_class_button = (By.XPATH, '//*[contains(@class,"ant-list-item")][' + index + ']/div[3]/button[1]')
        return goto_class_button

    # 备课按钮
    prepare_class_button = (By.XPATH, '//*[contains(@class,"ant-list-item")]/div[3]/button[2]')

    @staticmethod
    def get_prepare_class_button(index):
        prepare_class_button = (By.XPATH, '//*[contains(@class,"ant-list-item")][' + index + ']/div[3]/button[2]')
        return prepare_class_button

    # PPT列表
    ppt = (By.XPATH, '//*[@id="rootTeacher"]/div[2]/div/div/div/div[2]/div[2]/div/div/p')

    @staticmethod
    def select_ppt(index):
        ppt = (By.XPATH, '//*[@id="rootTeacher"]/div[2]/div/div/div/div[2]/div[2]/div/div/p[' + index + ']')
        return ppt

    # 今日课程icon
    today_course_icon = (By.XPATH, '//*[contains(@class,"ant-list-item-space")]/div[1]/a/img')

    @staticmethod
    def select_programme(index):
        today_course_icon = (By.XPATH, '//*[contains(@class,"ant-list-item")][' + index + ']/div[3]/button[1]]')
        return today_course_icon



