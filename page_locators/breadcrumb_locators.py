from selenium.webdriver.common.by import By


# 面包屑导航定位
class BreadcrumbLocators:
    # 可点击首页
    home_a = (By.XPATH, '//a[text()="首页"]')
    # 备授课
    class_list = (By.XPATH, '//span[contains(@class,"ant-breadcrumb-link")][text()="备授课"]')
    # 课程详情
    class_info = (By.XPATH, '//span[text()="课程详情"]')
    # 题库
    test_list = (By.XPATH, '//span[contains(@class,"ant-breadcrumb-link")][text()="题库"]')
    # 资源库
    resource_list = (By.XPATH, '//span[contains(@class,"ant-breadcrumb-link")][text()="资源库"]')
    # 试卷库
    exam_list = (By.XPATH, '//span[contains(@class,"ant-breadcrumb-link")][text()="试卷库"]')
    # 作业管理
    homework_manage = (By.XPATH, '//span[contains(@class,"ant-breadcrumb-link")][text()="作业管理"]')
    # 资源库课程详情
    resource_course_detail = (By.XPATH, '//span[contains(@class,"ant-breadcrumb-link")][text()="课程"]')
    # 布置作业详情
    assign_homework = (By.XPATH, '//span[contains(@class,"ant-breadcrumb-link")][text()="布置作业"]')
    # 组卷页面
    composition_test = (By.XPATH, '//span[contains(@class,"ant-breadcrumb-link")][text()="组卷"]')
    # 查看资源
    resource_info = (By.XPATH, '//span[text()="查看资源"]')
    # 试卷详情
    exam_info = (By.XPATH, '//span[text()="试卷详情"]')
    # 积分兑换
    points_change = (By.XPATH, '//span[text()="积分兑换"]')
    # 作业统计
    homework_statistics = (By.XPATH, '//span[text()="作业统计"]')
    # 可点击作业管理
    homework_manage_a = (By.XPATH, '//a[text()="作业管理"]')
    # 可点击资源库
    resource_a = (By.XPATH, '//a[text()="资源库"]')
    # 可点击试卷库
    exam_a = (By.XPATH, '//a[text()="试卷库"]')
    # 打印作业报告
    print_report = (By.XPATH, '//span[text()="打印报告"]')



