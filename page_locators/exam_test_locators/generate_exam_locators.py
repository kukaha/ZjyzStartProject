# coding: utf-8
from selenium.webdriver.common.by import By


class GenerateExamLocators:
    # 试题篮
    basket = (By.XPATH, '//div[@class="origin-paper"]')
    # 大题列表
    major_test_list = (By.XPATH, '//div[@id="paper-editing-content"]/div[2]/div')
    # alert提示
    alert = (By.XPATH, '//div[@class="ant-message-notice"]//span[2]')
    # 错误类型
    error_type = (By.XPATH, '//div[@class="ant-checkbox-group"]/label[1]/span[1]')
    error_description = (By.XPATH, '//textarea[@id="errorMemo"]')

    # @staticmethod
    # def major_test(div_index):
    #     """
    #     大题列表
    #     """
    #     major_test_list = (By.XPATH, '//div[@id="paper-editing-content"]/div[2]/div[' + div_index + ']')
    #     return major_test_list

    # # 组卷页面试题列表
    # test_list = (By.XPATH, '//div[@id="paper-editing-content"]/div/div/div[2]/div')

    @staticmethod
    def test_list(div_index):
        """
        小题列表
        @param：div_index：第几大题
        """
        test_list = (By.XPATH, '//div[@id="paper-editing-content"]/div[2]/div[' + div_index + ']/div[2]/div')
        return test_list

    @staticmethod
    def test_index(div_index, div_index1):
        """
        小题列表
        @param：div_index：第几大题；div_index1：第几小题
        """
        test_index = (By.XPATH, '//div[@id="paper-editing-content"]/div[2]/div[' + div_index + ']/div[2]/div[' + div_index1 + ']')
        return test_index

    @staticmethod
    def test_operation(div_index, div_index1, span_index):
        """
        试题操作项
        @param：div_index：第几大题；div_index1：第几小题；span_index：（1、答案解析2、编辑3、设定得分4、插入作答区5、收藏6、纠错7、删除）
        """
        test_operation = (By.XPATH, '//div[@id="paper-editing-content"]/div[2]/div[' + div_index + ']/div[2]/div[' + div_index1 + ']/div[3]/div/span[' + span_index + ']')
        return test_operation
    # 编辑试题详情
    edit_test_detail = (By.XPATH, '//div[@class="zwm-create-question-question"]')
    # 确定按钮
    confirm = (By.XPATH, '//span[text()="确 定"]')
    confirm1 = (By.XPATH, '//span[text()="确 认"]')
    # 设定得分输入框
    test_score = (By.XPATH, '//div[@class="ant-modal-body"]//form/div[1]/div/div[2]/div/span/input')
    # 作答区域是输入框
    input_answer_area = (By.XPATH, '//div[@class="ant-popover-inner"]//input')
    # 作答线
    answer_line = (By.XPATH, '//div[@class="Paper_rowsWithLine__1yPUK"]')
    answer_line_btn = (By.XPATH, '//div[@class="ant-popover-inner-content"]//button')
    # 删除试题提示
    delete_test_message = (By.XPATH, '//div[@class="ant-modal-confirm-content"]/div/div')