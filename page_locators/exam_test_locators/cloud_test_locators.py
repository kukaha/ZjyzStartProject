# coding: utf-8
from selenium.webdriver.common.by import By


class CloudTestLocators:
    # 题库导航
    question_bank = (By.XPATH, '//*[contains(@class,"teach-side-box ")][7]')
    # 试题列表
    test_list = (By.XPATH, '//ul[@class="ant-list-items"]/li/div')
    # alert提示
    alert = (By.XPATH, '//div[@class="ant-message-notice"]//span[2]')

    @staticmethod
    def test_year_info(li_index):
        """
        试卷年份列表
        @param：(li_index：题号）
        """
        test_year_info = (By.XPATH, '//ul[@class="ant-list-items"]/li[' + li_index + ']/div/div[1]')
        return test_year_info

    @staticmethod
    def test_year_info_tip(index):
        """
        试卷年份试卷标题
        @param：题号+7
        """
        test_year_info_tip = (By.XPATH, '//div[@id="root"]/following-sibling::div[' + index + ']/div/div/div/div[2]')
        return test_year_info_tip
    # 显示答案开关
    show_answer = (By.XPATH, '//div[@class="ant-switch-handle"]')

    @staticmethod
    def paper_basket(li_index):
        """
        添加试题篮按钮
        @param：li_index第几题
        """
        paper_basket = (By.XPATH, '//ul[@class="ant-list-items"]/li[' + li_index +']//button')
        return paper_basket

    @staticmethod
    def paper_basket_text(li_index):
        """
        添加试题篮按钮文案（试题篮/移出）
        @param：li_index第几题
        """
        paper_basket_text = (By.XPATH, '//ul[@class="ant-list-items"]/li[' + li_index + ']//button/div/span')
        return paper_basket_text

    @staticmethod
    def question_operation(li_index, span_index):
        """
        试题可操作项
        @param：（li_index：题号，span_index：操作项（1、解析2、纠错3、相似试题4、收藏））
        """
        operation = (By.XPATH, '//ul[@class="ant-list-items"]/li[' + li_index + ']/div/div[4]/div[2]/span[' + span_index + ']')
        return operation

    @staticmethod
    def use_question_num(index):
        """
        试题引用的总数
        """
        use_question_num = (By.XPATH, '//ul[@class="ant-list-items"]/li[' + index + ']/div/div[4]/div[1]/div/div/span[5]/span')
        return use_question_num
    # 引用试卷列表
    use_question = (By.XPATH, '//div[@class="ant-popover-inner-content"]/div')

    @staticmethod
    def basket_num(index):
        """
        试题篮总数（1、已添加的试题数2、试题篮添加总数）
        """
        basket_num = (By.XPATH, '//div[@class="basket-overview"]/div[1]/div[' + index + ']')
        return basket_num
    # 试卷标题
    test_paper_name = (By.XPATH, '//div[@class="test-paper"]/h1')
    # 返回按钮
    back_btn = (By.XPATH, '//div[@class="detail-head-tit"]/button')
    # 试题解析弹框
    question_analysis = (By.XPATH, '//div[@class="test-questions"]/div')
    # 关闭按钮
    close = (By.XPATH, '//div[@class="ant-modal-content"]/button')
    # 题型列表
    test_form_list = (By.XPATH, '//div[@class="resource-list-nav"]/div[1]/div/div/a')
    test_form_more = (By.XPATH, '//div[@class="resource-list-nav"]/div[1]/a')

    @staticmethod
    def test_form_a(li_index):
        """
        每个题型
        @param：index第几个题型
        """
        test_form_a = (By.XPATH, '//div[@class="resource-list-nav"]/div[1]/div/div/a[' + li_index + ']')
        return test_form_a




