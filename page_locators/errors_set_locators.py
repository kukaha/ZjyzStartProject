# coding=utf-8
# @time   :2021/08/26
# @Author :wushaotang
# @File   : errors_set_locators.py
# @Description: 中教云智慧教学平台-学生错题分析
# @Return  :
import re

from selenium.webdriver.common.by import By
from utils.UI.BasePage import BasePage


class ErrorsLocators(BasePage):


    #学生错题集菜单
    menu_errors =  (By.XPATH, '//*[@id="rootTeacher"]/section/aside/div[1]/div[1]/ul/li[9]/span')
    #左侧菜单’学生‘
    left_student =  (By.XPATH, '//main[@class="student-wrong-content"]//div[@class="ant-tabs-tab ant-tabs-tab-active"]')
    #左侧菜单’章节‘
    left_section = (By.XPATH,'//main[@class="student-wrong-content"]/div/div/div/div/div/div[2]')
    #左侧菜单’知识点‘
    left_point = (By.XPATH,'//main[@class="student-wrong-content"]/div/div/div/div/div/div[3]')