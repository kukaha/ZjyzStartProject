# coding: utf-8
from selenium.webdriver.common.by import By


class GenerateExamLocators:
    @staticmethod
    def generate_exam(index):
        """
        组卷按钮（1、手工组卷 2、智能组卷）
        """
        generate_exam_button = (By.XPATH, '//div[@class="btn-col"]/button[' + index + ']')
        return generate_exam_button

    @staticmethod
    def drop_down(index):
        """
        智能组卷方式（1、按章节组卷 2、按知识点组卷）
        """
        drop = (By.XPATH, '//div[contains(@class,"intelli-paper-btn-dropdown")]/ul/li[' + index + ']')
        return drop

    @staticmethod
    def select_catalog(index):
        """
        章节复选框列表
        """
        catalog_span = (By.XPATH, '//div[contains(@class,"ant-tabs-small")]//div[@class="ant-tree-list"]/div/div/div/div[' + index + ']/span[3]')
        return catalog_span
    # 知识点复选框列表
    knowledge_span1 = (By.XPATH, '//div[@class="KnowledgePointTreeCheckWrapper_TreeCheck__2fWYI"]//div[@class="ant-tree-list-holder-inner"]/div[1]')
    x = '//div[1]'
    # 精准出题
    accurate_tip = (By.XPATH, '//div[@class="PaperArgs_Container__EjDI9"]/div[1]/div[2]/div/label[1]')
    # 关联出题
    simple_tip = (By.XPATH, '//div[@class="PaperArgs_Container__EjDI9"]/div[1]/div[2]/div/label[2]')
    # 精准出题小问号图标
    accurate_tip_img = (By.XPATH, '//div[@class="PaperArgs_Container__EjDI9"]/div[1]/div[2]/div/img[1]')
    # 关联出题小问号图标
    simple_tip_img = (By.XPATH, '//div[@class="PaperArgs_Container__EjDI9"]/div[1]/div[2]/div/img[2]')
    # 题型列表
    test_form_list = (By.XPATH, '//div[@class="TypeSetView_view__3Xwwt"]/div')

    @staticmethod
    def tip_inner(index):
        """
        出卷方式提示：3、精准出题4、关联出题
        """
        inner = (By.XPATH, '//div[@id="rootTeacher"]/div[' + index + ']/div/div/div/div[2]')
        return inner

    @staticmethod
    def test_form(index):
        """
        题型
        """
        test_form = (By.XPATH, '//div[@class="TypeSetView_view__3Xwwt"]/div[' + index + ']')
        return test_form

    # 已选择的题型总数
    test_form_check = (By.XPATH, '//div[@class="TypeSetView_view__3Xwwt"]/div/span')
    # 交叉设置表头（题型、易、较易、中、较难、难、总题数）
    cross_set_table = (By.XPATH, '//table[@class="QuestionTableView_table__2QICf"]/th/td')
    # 交叉设置题型总数
    cross_set_count_input = (By.XPATH, '//table[@class="QuestionTableView_table__2QICf"]/tr/tr/td[7]')
    free_set_count_input = (By.XPATH, '//div[@class="QuestionSetView_data__3vdY-"]/div[1]/div[2]/div/span/div/div[2]')
    # 自由设置选项
    free_set_way = (By.XPATH, '//div[@class="PaperArgs_Container__EjDI9"]/div[3]/div[2]/div/div/div/div/label[2]')

    @staticmethod
    def cross_set_input(index):
        """
        交叉设置题型总数
        """
        set_test_count = (By.XPATH, '//table[@class="QuestionTableView_table__2QICf"]/tr/tr[' + index + ']/td[7]/div/div/div[2]/input')
        return set_test_count

    @staticmethod
    def free_set_input(index):
        """
        自由设置题型总数
        """
        set_test_count = (By.XPATH, '//div[@class="QuestionSetView_data__3vdY-"]/div[1]/div[2]/div[' + index + ']/span/div/div[2]/input')
        return set_test_count

    # 试题不足提示
    test_form_count_few = (By.XPATH, '//div[@class="ant-tooltip-inner"]')
    alert = (By.XPATH, '//div[@class="ant-message-notice"]//span[2]')
    generate_exam_btn = (By.XPATH, '//span[text()="生成试卷"]')
    # 试卷详情试题总数
    detail_test_count = (By.XPATH, '//div[@id="paper-editing-content"]/div/div/div[1]/h4')
    adjust_exam_btn = (By.XPATH, '//div[@class="Footer_Container__2Gv35"]/button[1]')
    # 试卷详情已选中的题型
    detail_select_test_form = (By.XPATH, '//div[@class="TypeSetView_view__3Xwwt"]/div[@class="TypeSetItem_select__1s7Aq"]')
    # 完成组卷
    complete_exam_btn = (By.XPATH, '//main[contains(@class,"ant-layout-content")]/div[2]/div/div[3]//button[2]')
    # 组卷提示
    tips = (By.XPATH, '//div[@class="ant-modal-confirm-body"]//div')
    # 确定按钮
    confirm = (By.XPATH, '//div[@class="ant-modal-confirm-btns"]/button[2]')
    # 试卷名称编辑按钮
    edit_exam_name = (By.XPATH, '//div[@class="ant-typography"]/div/span')
    # 试卷名称输入框
    input_exam_name = (By.XPATH, '//div[@class="PaperTitle_Title__35co-"]/div/div/textarea')
    # 我的试卷tab
    my_exam_tab = (By.XPATH, '//div[@class="ant-tabs-nav-list"]/div[3]/div')
    # 试卷详情试题总数
    detail_test_count = (By.XPATH, '//div[@id="paper-editing-content"]/div/div/div[1]/h4')
    adjust_exam_btn = (By.XPATH, '//div[@class="Footer_Container__2Gv35"]/button[1]')
    # 试卷详情已选中的题型
    detail_select_test_form = (By.XPATH, '//div[@class="TypeSetView_view__3Xwwt"]/div[@class="TypeSetItem_select__1s7Aq"]')
    # 完成组卷
    complete_exam_btn = (By.XPATH, '//main[contains(@class,"ant-layout-content")]/div[2]/div/div[3]//button[2]')
    # 组卷提示
    tips = (By.XPATH, '//div[@class="ant-modal-confirm-body"]//div')
    # 确定按钮
    confirm = (By.XPATH, '//div[@class="ant-modal-confirm-btns"]/button[2]')
    # 试卷名称编辑按钮
    edit_exam_name = (By.XPATH, '//div[@class="ant-typography"]/div/span')
    # 试卷名称输入框
    input_exam_name = (By.XPATH, '//div[@class="PaperTitle_Title__35co-"]/div/div/textarea')
    # 我的试卷tab
    my_exam_tab = (By.XPATH, '//div[@class="ant-tabs-nav-list"]/div[3]/div')