# coding: utf-8
from selenium.webdriver.common.by import By


class ExamInfoLocators:
    @staticmethod
    def exam_info_button(index):
        """
        试卷详情页按钮
        @param index: （1、显示答案 2、复制 3、添加到课程）
        @return:
        """
        button = (By.XPATH, '//div[@class="detail-head-edit"]//button[' + index + ']')
        return button
    # 复制试卷提示框
    copy_warn = (By.XPATH, '//div[@class="ant-modal-confirm-content"]')
    # 确定按钮
    confirm = (By.XPATH, '//span[text()="确 定"]')
    confirm2 = (By.XPATH, '//span[text()="确 认"]')
    # 返回按钮
    back = (By.XPATH, '//div[@class="ant-modal-footer"]/button[1]/span')
    # alert提示
    alert = (By.XPATH, '//div[@class="ant-message-notice"]//span[2]')
    # 大题总数
    major_test_count = (By.XPATH, '//div[@class="test-paper"]/div')
    # 跳转课程链接
    skip_link = (By.XPATH, '//div[@class="skip-link"]/span')
    # 课程名
    course_title = (By.XPATH, '//div[@class="curriculum-detail"]/header/div[1]/h2')
    # 我的课程
    my_course = (By.XPATH, '//div[@class="ant-modal-content"]//form/div[3]/div[2]/div/div/div')
    first_course = (By.XPATH, '//div[@id="courseId_list"]/following-sibling::div[1]/div[1]/div[1]/div/div/div')
    teaching_step_div = (By.XPATH, '//div[@class="ant-modal-content"]//form/div[4]/div[2]/div/div/div')
    teaching_step_input = (By.ID, 'stepId')
    # 教学环节总数
    teaching_step_count = (By.XPATH, '//div[@id="stepId_list"]/following-sibling::div[1]/div[1]/div/div/div')

    @staticmethod
    def teaching_step(index):
        """
        教学环节列表
        """
        teaching_step = (By.XPATH, '//div[@id="stepId_list"]/following-sibling::div[1]/div[1]/div/div/div[' + index + ']')
        return teaching_step

    @staticmethod
    def test_form(index):
        """
        大题题型
        @param index: 第index题
        @return: 大题题型
        """
        major_test_form = (By.XPATH, '//div[@class="test-paper"]/div[' + index + ']/h4')
        return major_test_form

    @staticmethod
    def test_count_text(index):
        """
        小题总数
        @param index: 第index个大题
        @return: 小题总数（共几题）
        """
        less_test_count_text = (By.XPATH, '//div[@class="test-paper"]/div[' + index + ']/h4/span')
        return less_test_count_text

    @staticmethod
    def test_count(index):
        """
        小题题目
        @param index: 第index个大题
        @return: 所有小题列表
        """
        test_count = (By.XPATH, '//div[@class="test-paper"]/div[' + index + ']/div/div')
        return test_count

    # 选择、解答题答案
    choice_question_answer = (By.XPATH, '//div[@class="test-paper"]//div[contains(@class,"active")]')
    # 填空题答案
    gap_answer = (By.XPATH, '//div[@class="test-paper"]/div/div//a[@class="gap-anwser"]')
