# coding: utf-8
from selenium.webdriver.common.by import By


class StageExamLocators:
    # 试卷库导航
    exam = (By.XPATH, '//*[contains(@class,"teach-side-box ")][6]')
    # 试卷总数
    exam_count = (By.XPATH, '//div[@class= "ant-list ant-list-split ant-list-something-after-last-item"]/div/div/ul/li')
    # 试卷标题
    exam_info_title = (By.XPATH, '//div[@class="test-paper"]/h1')
    # 我的课程
    my_course = (By.XPATH, '//div[@class="ant-modal-body"]/form/div[3]/div[2]/div/div/div/span')
    first_course = (By.XPATH, '//div[@id="courseId_list"]/following-sibling::div[1]/div[1]/div/div/div[1]/div')
    teaching_step_div = (By.XPATH, '//div[@class="ant-modal-body"]/form/div[4]/div[2]/div/div/div/span/div')
    # 教学环节输入框
    teaching_step_input = (By.XPATH, '//input[@id="stepId"]')
    teaching_step_count = (By.XPATH, '//div[@id="stepId_list"]/following-sibling::div[1]/div[1]/div/div/div/div')

    @staticmethod
    def teaching_step(index):
        """
        教学环节列表
        """
        teaching_step = (By.XPATH,
                         '//div[@id="stepId_list"]/following-sibling::div[1]/div[1]/div/div/div[' + index + ']/div')
        return teaching_step
    # 取消
    cancel = (By.XPATH, '//span[text()="取 消"]')
    # 确认按钮
    confirm = (By.XPATH, '//span[text()="确 认"]')
    # 跳转课程链接
    skip_course = (By.XPATH, '//div[@class="skip-coures"]/div[1]/span')
    # 跳转到课程页课程名
    course_name = (By.XPATH, '//div[@class="curriculum-detail"]/header/div[1]/h2')
    # alert提示
    alert = (By.XPATH, '//div[@class="ant-message-notice"]//span[2]')
    # 返回
    back = (By.XPATH, '//span[text()="返 回"]')

    @staticmethod
    def exam_list(index):
        """
        试卷列表
        @param index: 第index个试卷
        @return:
        """
        exam_list = (By.XPATH, '//div[@class= "ant-list ant-list-split ant-list-something-after-last-item"]/div/div/ul/li[' + index + ']/div/p/span')
        return exam_list

    @staticmethod
    def exam_operate(li_index, span_index):
        """
        @param li_index: 第li_index个试卷
        @param span_index: 1、布置作业2、打印3、添加课程4、添加收藏
        @return:
        """
        operate = (By.XPATH, '//div[@class= "ant-list ant-list-split ant-list-something-after-last-item"]/div/div/ul/li[' + li_index + ']/div/div/div[2]/span[' + span_index + ']')
        return operate
