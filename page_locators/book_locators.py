from selenium.webdriver.common.by import By


class BookLocators:
    # 数字教材导航
    e_book = (By.XPATH, '//*[contains(@class,"teach-side-box ")][4]')
    # 筛选
    screen = (By.XPATH, '//*[@id="sourcePopover"]/button')
    subject = (By.XPATH, '//*[@id="sourcePopover"][1]/button/span')
    class_button = (By.XPATH, '//*[@id="sourcePopover"][2]/button/span')
    edition = (By.XPATH, '//*[@id="sourcePopover"][3]/button/span')
    volume = (By.XPATH, '//*[@id="sourcePopover"][4]/button/span')
    # 教材学科
    book_subject = (By.XPATH, '//*[contains(@class,"material-breadth")]/div[1]//p[1]')
    # 教材版本
    book_edition = (By.XPATH, '//*[contains(@class,"material-breadth")]/div[1]//p[2]')
    # 教材册次
    book_volume = (By.XPATH, '//*[contains(@class,"material-breadth")]/div[1]/div[2]/div[2]/div')

    # 打开或下载教材
    open_book_button = (By.XPATH, '//*[contains(@class,"material-breadth")]/div/div[2]/div[2]/button')

    @staticmethod
    def select_book(index):
        open_book_button = (By.XPATH, '//*[contains(@class,"material-breadth")]/div[' + index + ']/div[2]/div[2]/button')
        return open_book_button

    # 选择学科
    @staticmethod
    def click_subject(index):
        click_subject = (By.XPATH, '//*[@id="sourcePopover"][1]//label[' + index + ']/span[2]')
        return click_subject

    # 选择年级
    @staticmethod
    def click_class(index):
        click_class = (By.XPATH, '//*[@id="rootTeacher"]/div[3]/div/div/div/div[2]/div/div/div/div/label[' + index + ']/span[2]')
        return click_class

    # 选择版本
    @staticmethod
    def click_edition(index):
        click_edition = (By.XPATH, '//*[@id="rootTeacher"]/div[2]/div/div/div/div[2]/div/div/div/div/label['+index+']/span[2]')
        return click_edition

    # 选择册次
    @staticmethod
    def click_volume(index):
        click_volume = (By.XPATH, '//*[@id="sourcePopover"]/div[2]/div/div/div/div[2]/div/div/div/div/label[' + index + ']/span[2]')
        return click_volume
