# coding: utf-8
from selenium.webdriver.common.by import By


class HomeworkLocators:
    # 作业管理导航
    homework_manage = (By.XPATH, '//*[contains(@class,"teach-side-box ")][3]')
    # 作业状态
    homework_status = (By.XPATH, '//div[contains(@class,"ant-table-fixed-column")]//tbody/tr[2]/td[1]/p/span[2]')
    # 作业未开始提示
    count_alert = (By.XPATH, '//span[text()="作业未开始，暂无统计数据"]')
    # 课程详情课程名
    course_info = (By.XPATH, '//*[@class="ant-layout-content teach-content"]//h2')
    # 总课程数
    course_count = (By.XPATH, '//div[contains(@class,"ant-table-fixed-column")]//tbody/tr')
    # 关闭按钮
    close = (By.XPATH, '//button[@class="ant-modal-close"]')
    # 资源关闭按钮
    resource_info_close = (By.XPATH, '//div[contains(@class,"openfile-model-resource")]//button')
    # 试卷关闭按钮
    exam_info_close = (By.XPATH, '//div[contains(@class,"homework-content-paper-modal-wrap")]/div/div[2]/button/span')
    # 取消打印
    cancel = (By.XPATH, '//span[text()="取 消"]')
    # 设置打印对象
    set_print_obj = (By.XPATH, '//div[text()="打印对象设置"]')
    # 按班发，选择班级
    set_by_class = (By.ID, 'assign-config_byClass')
    # 按小组和学生发，选择班级
    set_by_other = (By.XPATH, '//label[contains(@class,"school-class")]')
    # 确定按钮
    set_ok = (By.XPATH, '//span[text()="确 定"]')
    # 打印按钮
    print = (By.XPATH, '//span[text()="打 印"]')
    # 编辑作业发布成功提示
    issue_alert = (By.XPATH, '//span[text()="作编辑作业成功!"]')
    # 发布按钮
    issue = (By.XPATH, '//span[text()="发 布"]')
    # 删除作业提示
    delete_started_explain = (By.XPATH, '//div[text()="本次作业学生已经开始作答，确定要删除当前作业吗？"]')
    delete_not_started_explain = (By.XPATH, '//div[text()="确定要删除当前作业吗？"]')
    # 确定删除提示
    delete_alert = (By.XPATH, '//span[text()="删除作业成功"]')

    # 操作项
    @staticmethod
    def operate(tr_index, a_index):
        operate_button = (By.XPATH, '//div[contains(@class,"ant-table-fixed-column")]//tbody/tr['+tr_index+']/td[6]//a['+a_index+']')
        return operate_button

    # 获取名称（1、作业名2、课程名3、作答方式4、作业内容）
    @staticmethod
    def name(tr_index, a_index):
        get_name = (By.XPATH, '//div[contains(@class,"ant-table-fixed-column")]//tbody/tr['+tr_index+']/td['+a_index+']/p')
        return get_name

    # 作业发布状况（1、发布班级2、作业提交时间3、提交人数）
    @staticmethod
    def situation(tr_index, a_index):
        homework_situation = (By.XPATH, '//div[contains(@class,"ant-table-fixed-column")]//tbody/tr['+tr_index+']/td[5]//p/span['+a_index+']')
        return homework_situation

    # 作业内容弹框（1、资源名2、资源类型3、打印按钮）
    @staticmethod
    def content(index):
        homework_content = (By.XPATH, '//div[@class="ant-modal-content"]/div[2]/div[1]/div/div/div['+index+']')
        return homework_content

    # 发布给（1、按班发2、按小组发3、指定学生发）
    @staticmethod
    def issue_way(index):
        way_span = (By.XPATH, '//div[contains(@class,"ant-radio-group-outline")]/label['+index+']')
        return way_span
